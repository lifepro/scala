package adhoc

import org.lifepro.site.boot.LiftBootstrap
import scala.util.Random
import com.foursquare.rogue.LiftRogue._
import java.util.Date
import org.lifepro.site.model.{Material, User}
import net.liftweb.util.Props

/**
 * Generates material to simplify testing of the in-time data loading for the materials an
 * conclusions lists interfaces
 */
object GenTestMaterial extends App {
  LiftBootstrap.setupDB
  
  val ownerEmail = Props.get("default.user").openOrThrowException("default user needs to be set on lift properties")

  lazy val MILLIS_IN_DAY = 24 * 60 * 60 * 1000
  val maxDaysAgo = 30
  val maxMillisAgo = maxDaysAgo * MILLIS_IN_DAY
  val random = new Random()
  val owner = User.where(_.email eqs ownerEmail).get().getOrElse(sys.error("no user found"))

  def randomDate = {
    def now = new Date()
    val positiveRandom = random.nextLong() match {
      case v if v < 0 => -v
      case v => v
    }
    new Date(now.getTime - positiveRandom % maxMillisAgo)
  }

  def randomString(size:Int) = {
    val s = new StringBuilder
    for (i <- 0 to size)
      s.append(random.nextPrintableChar())
    s.toString()
  }

  // lets create some material at random
  for (i <- 1 to 200) {
    val rec = Material.createRecord.
      body(randomString(200)).
      owner(owner.id).
      modifiedTs(randomDate).
      save
  }

}
