package org.lifepro.site
package snippet

import net.liftweb.http.{S, DispatchSnippet}
import net.liftweb.util.Helpers._

object Resource extends DispatchSnippet {
  def dispatch = {
    case "script" => script
    case "css" => css
  }

  lazy val script =
    "* [src]" #> ("/js/" + S.attr("src").openOr("global.js")) &
      "* [type]" #> "text/javascript"

  lazy val css =
    "* [rel]" #> "stylesheet" &
      "* [type]" #> "text/css" &
      "* [href]" #> ("/css/" + S.attr("src").openOr("global.css"))
}
