package org.lifepro.site
package snippet

import net.liftweb.builtin.snippet.{Menu, Loc}
import net.liftweb.http.DispatchSnippet
import xml.NodeSeq
import net.liftweb.util.Helpers._

class Title extends DispatchSnippet {
  val SEPARATOR = " :: "

  def dispatch = {
    case "joined" =>
      Loc.dispatch("i") andThen
      ("* *" #> appendSeparator _) andThen
      Menu.dispatch("title")
  }

  private def appendSeparator(ns: NodeSeq) = ns.toString + SEPARATOR
}
