package org.lifepro.site
package snippet

import xml.{Text, Attribute, Elem, NodeSeq}
import net.liftweb.http.S
import net.liftweb.builtin.snippet.Loc

/**
 * A snippet which does localize all element attributes where it applied
 */
object LocAttrs {
  def i(ns:NodeSeq) = Loc.i(render(ns).collect { case e:Elem => e } .head)

  def render(ns:NodeSeq) = ns.collect {
    case e:Elem => e.attributes.map {
      case Attribute(name, value, next) =>
        val original = value.toString()
        val translated = trimWhitespace(S ? original)
        if (translated != original)
          Attribute(name, Text(translated), next)
        else
          Attribute(name, value, next)
      case a => a
    } .foldLeft(e) { _ % _ }
  }

  def trimWhitespace(s:String):String = {
    def trimRec(s:String, res:StringBuilder):String = {
      s match {
        case s if s.length == 0 => res.toString
        case s if s(0) <= ' ' => trimRec(s.dropWhile(_ <= ' '), res append ' ')
        case s => trimRec(s.drop(1), res append s.take(1))
      }
    }
    trimRec(s, new StringBuilder)
  }
}
