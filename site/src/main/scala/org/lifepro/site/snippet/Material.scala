package org.lifepro.site
package snippet

import scala.xml.NodeSeq
import net.liftweb.json.JsonAST._
import com.foursquare.rogue.LiftRogue._
import org.lifepro.site.model.{Material => MaterialEntity, User => UserEntity}
import net.liftweb.http.SessionVar
import org.joda.time.DateTime
import org.lifepro.site.utils.Unapply.BsonObjectId
import net.liftweb.common.{Failure, Full, Box}
import net.liftweb.util.Helpers
import com.foursquare.rogue._
import scala.Some
import com.foursquare.rogue.ExistsQueryClause
import com.foursquare.rogue.Query
import net.liftweb.json.JsonDSL
import net.liftweb.json.JsonAST.JObject
import net.liftweb.common.Full
import scala.Some
import com.foursquare.rogue.ExistsQueryClause
import net.liftweb.json.JsonAST.JArray
import net.liftweb.json.JsonAST.JField
import net.liftweb.json.JsonAST.JString
import com.foursquare.rogue.Query

object Material extends AjaxBinding {

  val PerPageLimit = 10

  object lastFetchedMaterial extends SessionVar[Option[MaterialEntity]](None)
  object lastFetchedConclusion extends SessionVar[Option[MaterialEntity]](None)

  def nextConclusions = nextResults(lastFetchedConclusion)(_.where(_.conclusionConfirmed eqs true))
  def nextProcessing = nextResults(lastFetchedMaterial)(_.where(_.conclusionConfirmed eqs false))

  def list: NodeSeq => NodeSeq = withUser {
    u =>
      addToScope {
        lastFetchedMaterial.set(None)
        JArray(
          u.definitionMaterial.asJValue :: nextProcessing
        )
      } andThen bindDataProcessor {
        case _ => JArray(nextProcessing)
      }
  }

  def conclusions: NodeSeq => NodeSeq = withUser {
    u =>
      addToScope {
        lastFetchedConclusion.set(None)
        JArray(nextConclusions)
      } andThen bindDataProcessor {
        case _ => JArray(nextConclusions)
      }
  }

  def removeHandler: NodeSeq => NodeSeq = withUser {
    u =>
      bindDataProcessor {
        case JObject(JField("remove", JString(BsonObjectId(id))) :: Nil) =>
          Box {
            userExistingMaterial(u).and(_._id eqs id).
              findAndModify(_.removed setTo true).updateOne()
          }.map(_ => true) ?~ "Already removed"
        case JObject(JField("restore", JString(BsonObjectId(id))) :: Nil) =>
          Box {
            userMaterial(u).and(_._id eqs id).and(_.removed eqs true).
              findAndModify(_.removed setTo false).updateOne()
          }.map(_ => true) ?~ "Already restored"
      }
  }

  def materialsHandler: NodeSeq => NodeSeq = withUser { u =>
    bindDataProcessor {
      case JArray(list) =>
        list.foldLeft[Box[List[MaterialEntity]]](Full(Nil)) {
          case (materials, JString(materialBody)) =>
            for {
              previous <- materials
              next <- Helpers.tryo {
                MaterialEntity.createRecord.body(materialBody).owner(u.id).save
              } ?~ "Database error"
            } yield next :: previous
          case _ => Failure("Invalid input")
        } .map { result =>
          if (result.size > 1) {
            JsonDSL.pair2jvalue(redirectTo(Service.MaterialList))
          }
          else result.map(_.asJValue).headOption.getOrElse(JNothing)
        }
    }
  }

  def withUser(f: UserEntity => (NodeSeq => NodeSeq)): NodeSeq => NodeSeq =
    User.currentUser.get.map(f).openOr(identity)

  protected def userMaterial(u: UserEntity) = MaterialEntity.where(_.owner eqs u.id)

  protected def userExistingMaterial(u: UserEntity) = userMaterial(u).and(_.removed eqs false)

  protected type MaterialQuery = Query[MaterialEntity, MaterialEntity, Unordered with Unlimited]

  protected def nextResults(
    lastMaterial: SessionVar[Option[MaterialEntity]]
  )(
    modifier: MaterialQuery => MaterialQuery
  ) = User.currentUser.get.map {
    u =>
      val results = {
        val myMaterial = modifier(userExistingMaterial(u))
        lastMaterial.get.map {
          m =>
            myMaterial.where(_.modifiedTs lt new DateTime(m.modifiedTs.get))
        }.getOrElse(myMaterial)
      }.orderDesc(_.modifiedTs).limit(PerPageLimit).fetch()

      results.lastOption.foreach {
        m =>
          lastMaterial.set(Some(m))
      }
      results.map(_.asJValue)
  }.openOr(Nil)

}
