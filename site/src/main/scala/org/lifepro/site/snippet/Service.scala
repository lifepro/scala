package org.lifepro.site
package snippet

import net.liftweb.sitemap._
import net.liftweb.http.S
import net.liftweb.common.{Full, LazyLoggable}
import net.liftweb.util.Helpers
import org.lifepro.site.service.RecentSection.DefaultRecentSection
import org.lifepro.site.service.RecentSection

object Service extends LazyLoggable with AjaxBinding {

  import net.liftweb.util.Helpers._
  import org.lifepro.site.utils.CollectionHelpers._

  val Service = "Service"

  val ConclusionsList = "Conclusions List"
  val MaterialList = "Material List"
  val Definition = "Diagnostics Definition"
  val Instruments = "Instruments"

  val siteMenu =
    Menu.i(Service) / "service" / ** withChildren(
      Menu.i("Diagnostics") / "diagnostics" / ** >> DefaultRecentSection(Definition) withChildren(
        Menu.i("Diagnostics Instructions") / "index",
        Menu.i(Definition) / "definition"
        ) ,
      Menu.i("Material") / "material" / ** >> DefaultRecentSection(MaterialList) withChildren(
        Menu.i("Material Instructions") / "index",
        Menu.i(MaterialList) / "list"
        ),
      Menu.i(Instruments) / "instruments" / ** withChildren(
        Menu.i("Instruments Instructions") / "index",
        ScaleUp.menuable,
        TT.menuable
        ),
      Menu.i("Conclusions") / "conclusions" / ** >> DefaultRecentSection(ConclusionsList) withChildren(
        Menu.i("Conclusions Instructions") / "index",
        Menu.i(ConclusionsList) / "list"
        )
      )

  def boot() {
    snippet.Definition.boot()
    ScaleUp.boot()
    TT.boot()
  }

  lazy val flatSiteMenu = {
    def flat(m: Menu): List[Menu] =
      m :: m.kids.toList.flatMap(flat _)
    flat(siteMenu.toMenu)
  }

  val menu = "a" #> flatSiteMenu.flatMap { m =>
    val level = S.attr("level").map(_.toInt).openOr(10)
    val loc = m.loc
    for {
      request <- S.request.toList if
    (request.path.partPath countEqualWith loc.link.uriList) >= level - 1 &&
      loc.link.uriList.length == level
    } yield {
      loc.asInstanceOf[Loc[Any]].requestValue.doWith(Full(null)) {
        val recent = S.attr("recent").map(Helpers.toBoolean).openOr(false)
        val href = loc.calcDefaultHref match {
          case href =>
            val slash = if (href.endsWith("/")) "" else "/"
            val addition = if (recent) slash + RecentSection.RECENT_PATH_TRIGGER else ""
            href + addition
        }
        val text = loc.linkText
        "* [href]" #> href & "* *" #> text & "* [title]" #> text & "* [data-toggle]" #> "tooltip"
      }
    }
  }

  def estimations = "button *" #>
    (for {
      from <- S.attr("from").flatMap(v => Helpers.tryo { Integer.parseInt(v) }) or Full(0)
      to <- S.attr("to").flatMap(v => Helpers.tryo { Integer.parseInt(v) }) or Full(10)
    } yield (from to to).toList).openOrThrowException("should never be thrown")
}
