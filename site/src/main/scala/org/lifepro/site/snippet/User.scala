package org.lifepro.site
package snippet

import net.liftweb.util.Helpers._
import org.lifepro.site.model.{PasswordResetRequest, User => UserEntity, Registration}
import org.lifepro.site.utils.MenuHelpers
import com.foursquare.rogue.LiftRogue._
import net.liftweb.util.{Props, Mailer}
import net.liftweb.sitemap._
import org.bson.types.ObjectId
import net.liftweb.json.JsonAST._
import net.liftweb.common._
import net.liftweb.http.{S, SessionVar}
import Box._
import net.liftweb.json.JsonAST.JObject
import net.liftweb.common.Full
import scala.Some
import net.liftweb.json.JsonAST.JField
import net.liftweb.sitemap.Loc.LocGroup
import net.liftweb.json.JsonAST.JBool
import net.liftweb.json.JsonAST.JString
import scala.xml.{Text, Elem}
import org.joda.time.DateTime

object User extends LazyLoggable with AjaxBinding with MenuHelpers {

  lazy val siteMenu = List(continueRegistrationMenu, passwordResetMenu)

  lazy val continueRegistrationMenu = Menu.param[Registration]("Register",
    Loc.LinkText(r => Text(S.?("Registration continue %s", r.email))),
    s => Registration.where(_._id eqs ObjectId.massageToObjectId(s)).get(),
    r => r.id.toString
  ) / "register" >> LocGroup("service")

  lazy val passwordResetMenu = Menu.param[PasswordResetRequest]("Reset password",
    Loc.LinkText(r => Text(S.?("Password reset %s", r.email))),
    s => PasswordResetRequest.where(_._id eqs ObjectId.massageToObjectId(s)).get(),
    r => r.id.toString
  ) / "reset" >> LocGroup("service")

  def menu = "*" #> {
    ((currentUser.get, currentReg or currentReset) match {
      case (Empty, Empty) => embed("sign_in")
      case (Full(user), Empty) => embed("signed_in")
      case _ => <div><a class="btn btn-link" href="/" data-lift="Loc.i">Main</a></div>
    }) collect {
      case e: Elem =>
        e % classAppend(e, "user-menu")
    }
  }

  def email = "* [value]" #> (currentReg.map(_.email.is) or currentReset.map(_.email.is))

  def greeting = "* *" #> {
    for {
      user <- currentUser.get
    } yield {
      val userName = user.firstName.is + " " + user.lastName.is
      S.?("Hello user", userName)
    }
  }

  def loginHandler = {
    logger.debug("Adding user login handler")
    bindDataProcessor {

      // login request
      case JObject(JField("email", JString(email)) :: JField("password", JString(password)) :: _) =>
        logger.debug("Got login request for email " + email)
        for {
          user <- UserEntity.where(_.email eqs email).and(_.password eqs password).
            findAndModify(_.lastLoggedIn setTo new DateTime()).updateOne() ?~ "Bad credentials"
        } yield {
          currentUser.set(Full(user))
          redirectTo(Service.MaterialList)
        }

      // reset password request
      case JObject(JField("email", JString(email)) :: JField("restore", JBool(true)) :: _) =>
        logger.debug("Got password restore request for email: " + email)
        for {
          user <- (UserEntity.where(_.email eqs email).get(): Box[UserEntity]) ?~ "E-mail not registered"
          request <- PasswordResetRequest.where(_.email eqs email).get() or
            Full(PasswordResetRequest.createRecord.email(email).save)
          _ <- sendMail(
            user.email.is,
            S ? "Password reset",
            S.?("Password reset e-mail body",
              host, s"$host${passwordResetMenu.toLoc.calcHref(request)}")
          )
        } yield HtmlResource("Check your e-mail box for password")

      // register request
      case JObject(JField("email", JString(email)) :: _) =>
        logger.debug("Creating registration for email: " + email)
        for {
          _ <- UserEntity.where(_.email eqs email).get() match {
            case Some(_) => Failure("E-mail already registered")
            case None => Full(())
          }
          registration <- Registration.where(_.email eqs email).get() or
            Full(Registration.createRecord.email(email).save)
          _ <- sendMail(
            registration.email.is,
            S.?("Lifepro registration"),
            S.?("Finalize registration e-mail body",
              host, s"$host${continueRegistrationMenu.toLoc.calcHref(registration)}")
          )
        } yield HtmlResource("Check your e-mail box")
    }
  }

  def registerHandler = {
    logger.debug("Attaching register handler")

    bindDataProcessor {
      case JObject(JField("firstName", JString(firstName)) ::
        JField("lastName", JString(lastName)) ::
        JField("password", JString(password)) :: _) =>

        for {
          registration <- currentReg
        } yield {
          val user = UserEntity.createRecord.email(registration.email.is).
            firstName(firstName).
            lastName(lastName).
            password(password).save
          currentUser.set(Full(user))
          registration.delete_!
          logger.info("Created user " + user.id)
          redirectTo(Service.MaterialList)
        }
    }
  }

  def passwordResetHandler = {
    logger.debug("Attaching password reset handler")

    bindDataProcessor {
      case JObject(JField("password", JString(password)) :: _) =>
        for {
          resetRequest <- currentReset
          user <- (UserEntity.where(_.email eqs resetRequest.email.is).
            findAndModify(_.password setTo password).updateOne(true): Box[UserEntity])
        } yield {
          currentUser.set(Full(user))
          resetRequest.delete_!
          logger.info("Set new password for user " + user.id)
          redirectTo(Service.MaterialList)
        }
    }
  }

  def logoutHandler = {
    logger.debug("Attaching logout handler")

    bindDataProcessor {
      case _ =>
        currentUser.set(Empty)
        Full(redirectTo("Main"))
    }
  }

  object currentUser extends SessionVar[Box[UserEntity]](Empty)

  private def currentReg = continueRegistrationMenu.toLoc.referredValue

  private def currentReset = passwordResetMenu.toLoc.referredValue

  private def host = S.hostAndPath

  private def sendMail(recipient: String, subject: String, body: String) =
    for {
      smtpUser <- Props.get("mail.smtp.user")
      from = Mailer.From(smtpUser, Full("Lifepro"))
    } yield Mailer.sendMail(
      from,
      Mailer.Subject(subject),
      Mailer.PlainMailBodyType(body),
      Mailer.To(recipient)
    )
}
