package org.lifepro.site
package snippet

import net.liftweb.http.S
import net.liftweb.json.JsonAST._
import scala.Some
import net.liftweb.util.Helpers
import org.bson.types.ObjectId
import net.liftweb.common.Box
import com.foursquare.rogue.LiftRogue._
import model.{Material => MaterialEntity}
import org.lifepro.site.service.RecentSection

/**
 * General scale rising process
 */
object ScaleUp extends Process {

  lazy val Name = "Scale Up"
  lazy val MenuPath = "scale_up"

  object Params extends Enumeration {
    val Intro = Value("intro")
    val Parent = Value("parent")
  }

  object Intro extends Enumeration {
    val SelectAreaOfLife = Value("Select area of life")
    val GetBackToTheMoment = Value("Get back to the moment")
    val WhatYouNeedToChange = Value("What you need to change?")
    val WhatWillReallyHappen = Value("What will really happen?")

    val Default = SelectAreaOfLife
  }

  def createLink(intro:Option[Intro.Value] = None, parent:Option[MaterialEntity] = None) = {
    Map(Params.Intro -> intro.map(_.id), Params.Parent -> parent.map(_.id)).foldLeft[Option[String]](None) {
      case (r, (param, valueOpt)) =>
        valueOpt.flatMap { value =>
          val paramValue = s"$param=$value"
          r.map(_ + "&" + paramValue) orElse Some(paramValue)
        } orElse r
    }
  }

  def requestedIntro =
    for {
      request <- S.request
      intros <- request.params.get(Params.Intro.toString) orElse Some(Nil)
      intro <- intros.headOption orElse Some(Intro.Default.id.toString)
      idx <- Helpers.tryo(intro.toInt)
      intro <- Helpers.tryo {
        Intro(idx)
      }
    } yield intro.toString

  def parentId =
    for {
      request <- S.request
      parents <- request.params.get(Params.Parent.toString)
      parent <- parents.headOption
      parentId <- Box !! ObjectId.massageToObjectId(parent)
    } yield parentId

  override val onMaterialCreated = Some { m: MaterialEntity =>
    m.gsrp.get.intro.set(requestedIntro)
    m.parent.set(for {
      u <- User.currentUser.get
      parent <- parentId
      m <- MaterialEntity.where(_.owner eqs u.id).and(_._id eqs parent).and(_.removed eqs false).get()
    } yield m.id.toString
    )
    ()
  }

  def startMaterial(m: MaterialEntity): Unit = m.gsrp.get.started.set(Some(true))

  def feelingsHandler = {
    autoSaveProcessor { text =>
      for {
        material <- location.referredValue
      } yield {
        material.refresh.gsrp.get.feelings.set(Some(text))
        material.save
        JNothing
      }
    }
  }

  def wishesHandler = {
    autoSaveProcessor { text =>
      for {
        material <- location.referredValue
      } yield {
        material.refresh.gsrp.get.wishes.set(Some(text))
        material.save
        JNothing
      }
    }
  }

  def estimationHandler = {
    bindDataProcessor {
      case JInt(estimation) =>
        for {
          material <- location.referredValue
        } yield {
          material.refresh.gsrp.get.estimation.set(Some(estimation.toInt))
          material.save
          JNothing
        }
    }
  }

  def mostUnpleasantHandler = {
    autoSaveProcessor { text =>
      for {
        material <- location.referredValue
      } yield {
        material.refresh.gsrp.get.mostUnpleasant.set(Some(text))
        material.save
        JNothing
      }
    }
  }

  def mostUnpleasantConfirmHandler = {
    bindDataProcessor {
      case _ =>
        for {
          material <- location.referredValue
        } yield {
          material.refresh.gsrp.get.mostUnpleasantConfirmed.set(Some(true))
          material.save
          JNothing
        }
    }
  }

  def whatToChangeHandler = {
    autoSaveProcessor { text =>
      for {
        material <- location.referredValue
      } yield {
        material.refresh.gsrp.get.whatToChange.set(Some(text))
        material.save
        JNothing
      }
    }
  }

  def whatToChangeConfirmHandler = {
    bindDataProcessor {
      case _ =>
        for {
          material <- location.referredValue
        } yield {
          material.refresh.gsrp.get.whatToChangeConfirmed.set(Some(true))
          material.save
          JNothing
        }
    }
  }

  def couldChangeHandler = {
    bindDataProcessor {
      case JBool(could) =>
        for {
          material <- location.referredValue
        } yield {
          material.refresh.save

          val nextMaterial = if (!could) {
            createSavedMaterial(Intro.WhatYouNeedToChange, parent = Some(material)).toOption
          }
          else {
            val previousBody = material.body.get
            material.refresh.gsrp.get.clear().intro(Intro.GetBackToTheMoment.toString)
            material.previousBody.set(Some(previousBody))
            material.body.set("")
            material.bodyConfirmed.set(false)
            material.save
            None
          }

          nextMaterial match {
            case Some(m) =>
              Map(redirectTo(Name, m))
            case None =>
              Map(redirectTo(Service.Instruments, true))
          }
        }
    }
  }

  def fromDefinition = {
    bindDataProcessor {
      case JString(problem) =>
        for {
          material <- createSavedMaterial(Intro.WhatWillReallyHappen, body = Some(problem))
        } yield redirectTo(Name, material)
    }
  }

  protected def createSavedMaterial(intro:Intro.Value, body:Option[String] = None, parent:Option[MaterialEntity] = None) =
    for {
      user <- User.currentUser.get
    } yield {
      val material = MaterialEntity.createRecord.owner(user.id)
      onMaterialCreated.foreach(_(material))
      startMaterial(material)
      material.gsrp.get.intro.set(Some(intro.toString))
      body.foreach(v => material.body(v))
      material.parent(parent.map(_.id.toString))
      material.save
    }

  override protected def conclusionRedirect: (String, JValue) = {
    linkTo(Name, Some(null)).foreach(RecentSection.visit)
    (for {
      material <- location.referredValue
      introText <- material.gsrp.get.intro.get
      parent = material.parentMaterial
      redirect <- Intro.withName(introText) match {
        case Intro.WhatWillReallyHappen =>
          Some(redirectTo(Service.Definition))
        case _ if parent.isDefined =>
          parent.map(redirectTo(Name, _))
        case _ =>
          None
      }
    } yield redirect) openOr super.conclusionRedirect
  }
}
