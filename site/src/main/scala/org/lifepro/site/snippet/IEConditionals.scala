package org.lifepro.site.snippet

import scala.xml.Unparsed
import net.liftweb.http.S
import org.lifepro.site.utils.XmlHelpers
import net.liftweb.util.Html5

object IEConditionals extends XmlHelpers {
  def render = Unparsed(
    s"""
    |<!--[if IE]>
    |    ${S.withAttrs("src" -> "ie.css") { Resource.css(<link/>) } .map(Html5.toString).reduceLeft(_ + _)}
    |<![endif]-->
    |<!--[if IE 7]>
    |    <link rel="stylesheet" href="/css/fontello-ie7.css">
    |<![endif]-->
    """.stripMargin
  )
}
