package org.lifepro.site
package snippet

import net.liftweb.common.{LazyLoggable, Empty, Full, Box}
import net.liftweb.sitemap.{Menu, Loc}
import net.liftweb.http.{RequestVar, S, LiftRules, SessionVar}
import model.{Material => MaterialEntity}
import org.bson.types.ObjectId
import com.foursquare.rogue.LiftRogue._
import scala.xml.Text
import org.lifepro.site.service.UniversalRedirect
import net.liftweb.json.JsonAST.JNothing
import org.lifepro.site.utils.{MenuHelpers}
import net.liftweb.json.JValue

/**
 * A base trait for TT or GSRP processes
 */
trait Process extends AjaxBinding with LazyLoggable with MenuHelpers {

  val New = "new"

  val Name:String
  val MenuPath:String

  val onMaterialCreated:Option[MaterialEntity => Unit] = None

  def startMaterial(m:MaterialEntity)

  lazy val menuable = Menu.param[MaterialEntity](Name, S.loc(Name, Text(Name)), decoder, encoder) / MenuPath

  lazy val decoder:String => Box[MaterialEntity] = { s =>
    for {
      user <- User.currentUser.get
      req <- S.request
      material <- {
        if (s == New) {
          newMaterial.get.filter(_ => req.ajax_?) or {
            needRedirect.set(true)
            newMaterial.set(Full {
              val m = MaterialEntity.createRecord.owner(user.id)
              onMaterialCreated.foreach(_(m))
              m
            })
          }
        } else {
          val curId = ObjectId.massageToObjectId(s)
          newMaterial.set(newMaterial.get.filter(_.id == curId)) or
            MaterialEntity.where(_._id eqs curId).and(_.owner eqs user.id).get()
        }
      }
    } yield material
  }

  lazy val encoder:MaterialEntity => String = { m =>
    (for {
      material <- Box !! m
    } yield material.id.toString) openOr New
  }

  lazy val location = LiftRules.siteMap.flatMap(_.findLoc(Name).asInstanceOf[Box[Loc[MaterialEntity]]]).get
  def currentMaterial = location.currentValue

  /**
   * A material which is being created by opening /new
   */
  object newMaterial extends SessionVar[Box[MaterialEntity]](Empty)
  object needRedirect extends RequestVar[Boolean](false)

  def boot() {
    UniversalRedirect.append {
      case req if location.doesMatch_?(req) && needRedirect.get =>
        for {
          material <- newMaterial.get
        } yield location.calcHref(material)
    }
  }

  def attachMaterial = {
    addToScope {
      currentMaterial.map(_.asJValue).openOr(JNothing)
    }
  }

  def bodyHandler = autoSaveProcessor { text =>
    for {
      material <- location.referredValue
    } yield {
      material.body(text).refresh
      startMaterial(material)
      material.save
      newMaterial.set(Empty)
      JNothing
    }
  }

  def bodyConfirmHandler = {
    bindDataProcessor {
      case _ =>
        for {
          material <- location.referredValue
        } yield {
          material.bodyConfirmed(true).refresh
          startMaterial(material)
          material.save
          JNothing
        }
    }
  }

  def conclusionHandler = {
    autoSaveProcessor { text =>
      for {
        material <- location.referredValue
      } yield {
        material.refresh.conclusion(Some(text))
        material.save
        JNothing
      }
    }
  }

  def conclusionConfirmHandler = {
    bindDataProcessor {
      case _ =>
        for {
          material <- location.referredValue
        } yield {
          material.refresh.conclusionConfirmed(true)
          material.save
          Map(
            ContextVarName -> material.asJValue.reduce(material.conclusionConfirmed.name),
            conclusionRedirect
          )
        }
    }
  }
  
  protected def conclusionRedirect: (String, JValue) = redirectTo(Service.ConclusionsList)

}
