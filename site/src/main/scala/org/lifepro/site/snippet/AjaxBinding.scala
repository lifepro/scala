package org.lifepro.site
package snippet

import net.liftweb.http.{LiftRules, S}
import net.liftweb.http.js.JsCmd
import net.liftweb.http.js.JsExp._
import net.liftweb.common._
import net.liftweb.util.{Helpers, Html5}
import org.lifepro.site.utils.{MenuHelpers, JsonHelpers, XmlHelpers}
import scala.xml._
import net.liftweb.builtin.snippet.Embed
import net.liftweb.json._
import net.liftweb.http.js.JE.JsFunc
import net.liftweb.common.Full
import net.liftweb.http.js.JE.JsVar
import net.liftweb.http.js.JE.JsRaw
import net.liftweb.json.JsonAST.JString
import org.lifepro.site.service.RecentSection

trait AjaxBinding extends XmlHelpers with MenuHelpers with JsonHelpers with LazyLoggable {

  val ContextVarName = "context"
  val RedirectTo = "redirectTo"

  implicit val formats = DefaultFormats + HtmlResource.serializer

  /**
   * Adds a funcId and ServerRoundtip controller to the element's scope
   * @param f funcId function handler
   * @param formats
   * @return
   */
  def bindDataProcessor(f: PartialFunction[JValue, BoxOrRaw[JValue]])(implicit formats: Formats): NodeSeq => NodeSeq =
    _.collect {
      case e: Elem =>
        val (jsCall, _) = S.createJsonFunc {
          handlerWrapper(f)
        }
        val setScopeFuncId = (JsRaw("funcId") === JString(jsCall.funcId)).cmd
        e % ngInitAppend(e, setScopeFuncId.toJsCmd) % ("ng-controller" -> "ServerRoundtrip")
    }

  /**
   * Binds a data processor which expects a string to be passed to body
   * @param body a handler which takes string and returns a response, successful (i.e. full box) response
   *             is ignored
   * @return a snippet to bind to an HTML element
   */
  def autoSaveProcessor(body: String => BoxOrRaw[JValue]) =
    bindDataProcessor {
      case JString(text) =>
        for {
          _ <- body(text)
        } yield HtmlResource("Changes saved")
    }


  /**
   * Adds a JValue to angular's element scope (by the using ng-init)
   * also adds ng-show and "display:none" style to hide element before angular initializes the scope
   * Default scope variable name is "context" optional snippet parameter "var" could be used to change it to another
   * value
   * @param jValue JValue to add to scope (JNothing will be just skipped)
   * @return a snippet doing necessary DOM transformation
   */
  def addToScope(jValue: JValue): NodeSeq => NodeSeq =
    _.collect {
      case e: Elem =>
        val varName = S.attr("var") openOr ContextVarName
        (jValue.toOpt.map {
          jv =>
            JsRaw(varName) === jv
        }.map(_.cmd.toJsCmd) match {
          case Some(cmd) =>
            e % ngInitAppend(e, cmd)
          case None => e
        }) % ngCloak(e)
    }

  protected lazy val ngInitAppend = appendToAttr("ng-init", ";") _
  protected lazy val classAppend = appendToAttr("class", " ") _

  protected def ngCloak(e: Elem) = classAppend(e, "ng-cloak") append appendToAttr("ng-cloak")(e, Empty)

  protected def embed(what: String) = S.withAttrs("what" -> what)(Embed.render(NodeSeq.Empty))

  protected def redirectTo(locName: String, recent:Boolean = false) = _redirectTo(locName, None, recent)

  protected def redirectTo[T](locName:String, value: T, recent:Boolean = false) =
    _redirectTo(locName, Some(value), recent)

  private def _redirectTo[T](locName: String, value:Option[T], recent:Boolean) =
    RedirectTo -> linkTo(locName, value).map { v =>
      val slash:String = if (v.endsWith("/")) "" else "/"
      if (recent) v + slash + RecentSection.RECENT_PATH_TRIGGER else v
    } .map(JString).openOr(JNothing)

  private def handlerWrapper(f: PartialFunction[JValue, BoxOrRaw[JValue]])(implicit formats: Formats) =
    new PartialFunction[JValue, JsCmd] {
      def isDefinedAt(x: JValue) = f.isDefinedAt(x)

      def apply(v: JValue) = {
        val result = unboxJValue(f(v))
        (JsVar("angular") ~> JsFunc("noop", result)).cmd
      }
    }

  private def unboxJValue(v: Box[JValue])(implicit formats: Formats): JValue = v match {
    case Full(jv) =>
      Map("success" -> true, "result" -> jv)
    case Failure(msg, _, _) =>
      Map("success" -> false, "result" -> HtmlResource(msg))
    case Empty =>
      Map("success" -> false)
  }

}
