package org.lifepro.site.snippet

import net.liftweb.http.{LiftRules, S, DispatchSnippet}
import scala.xml.{Text, NodeSeq}
import net.liftweb.util.Helpers._
import java.nio.charset.Charset
import scala.io.Codec
import org.lifepro.site.utils.StreamHelpers
import java.io.{StringWriter}

object AngularTemplate extends DispatchSnippet with StreamHelpers {
  def dispatch: AngularTemplate.DispatchIt = {
    case name => processLiftOnInner(name)
  }

  private def processLiftOnInner(templateName:String): NodeSeq => NodeSeq =
    "* [id]" #> templateName &
    "* [type]" #> "text/ng-template" &
    "* *" #> { ns: NodeSeq =>
      ns.map {
        case t@Text(text) =>
          implicit val c = Codec(Charset.forName("UTF-8"))

          val liftProcessed = for {
            req <- S.request
            htmlProps <- LiftRules.htmlProperties.make.map(_(req))
            elem <- htmlProps.htmlParser(text.toInputStream)
            session <- S.session
            processed = session.processSurroundAndInclude("", elem)
            processedString = {
              val writer = new StringWriter
              htmlProps.htmlWriter(processed.head, writer)
              writer.toString
            }
          } yield Text(processedString)

          liftProcessed openOr t
        case e => e
      }
    }
}
