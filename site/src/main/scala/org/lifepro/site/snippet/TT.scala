package org.lifepro.site.snippet

import org.lifepro.site.model.{Material => MaterialEntity}
import net.liftweb.json.JsonAST._
import scala.Some
import org.lifepro.site.service.RecentSection

/**
 * TT process
 */
object TT extends Process {
  
  lazy val Name = "TT"
  lazy val MenuPath = "tt"

  def startMaterial(m: MaterialEntity): Unit = m.tt.get.started.set(Some(true))

  def myFeelingsHandler = autoSaveProcessor { text =>
    for {
      material <- location.referredValue
    } yield {
      material.refresh.tt.get.myFeelings.set(Some(text))
      material.save
      JNothing
    }
  }

  def myWishesHandler = autoSaveProcessor { text =>
    for {
      material <- location.referredValue
    } yield {
      material.refresh.tt.get.myWishes.set(Some(text))
      material.save
      JNothing
    }
  }

  def confirmMineHandler = bindDataProcessor {
    case _ =>
      for {
        material <- location.referredValue
      } yield {
        material.refresh.tt.get.mineConfirmed.set(Some(true))
        material.save
        ContextVarName -> material.asJValue.reduce(
          material.tt.name, material.tt.get.mineConfirmed.name)
      }
  }

  def hisFeelingsHandler = autoSaveProcessor { text =>
    for {
      material <- location.referredValue
    } yield {
      material.refresh.tt.get.hisFeelings.set(Some(text))
      material.save
      JNothing
    }
  }

  def hisWishesHandler = autoSaveProcessor { text =>
    for {
      material <- location.referredValue
    } yield {
      material.refresh.tt.get.hisWishes.set(Some(text))
      material.save
      JNothing
    }
  }

  def confirmHisHandler = bindDataProcessor {
    case _ =>
      for {
        material <- location.referredValue
      } yield {
        material.refresh.tt.get.hisConfirmed.set(Some(true))
        material.save
        ContextVarName -> material.asJValue.reduce(
          material.tt.name, material.tt.get.hisConfirmed.name)
      }
  }

  override protected def conclusionRedirect = {
    linkTo(Name, Some(null)).foreach(RecentSection.visit)
    super.conclusionRedirect
  }
}
