package org.lifepro.site
package snippet

import org.lifepro.site.model.{User => UserEntity}
import net.liftweb.json.JsonAST._
import com.foursquare.rogue.LiftRogue._
import net.liftweb.json.JsonAST.JField
import net.liftweb.json.JsonAST.JObject
import net.liftweb.json.JsonAST.JString
import scala.Some
import net.liftweb.common.LazyLoggable
import net.liftweb.http.LiftRules
import org.lifepro.site.service.UniversalRedirect
import org.lifepro.site.utils.MenuHelpers

object Definition extends LazyLoggable with AjaxBinding with MenuHelpers {

  def location(name: String) = LiftRules.siteMap.flatMap(_.findLoc(name))

  lazy val definitionLocation = location("Diagnostics Definition")
  lazy val definitionWorkoutUrl = definitionLocation.toOption.flatMap(_.defaultLink).map(_ + "?restart")

  def boot() {
    UniversalRedirect.append {
      case req if definitionLocation.map(_.doesMatch_?(req)).openOr(false)
        && req.param("restart").isDefined =>
        User.currentUser.doSync {
          userBox.map {
            u =>
              u.definition.is.completed(None).estimation(None)
              UserEntity.where(_._id eqs u.id).modify(_.definition setTo u.definition.is).updateOne()
              req.path.partPath.mkString("/", "/", "")
          }
        }
    }
  }

  def textHandler = {
    logger.debug("Attaching definition handler")
    addToScope {
      User.currentUser.get.map {
        u =>
          u.definition.asJValue
      }.openOr(JNothing)
    } andThen bindDataProcessor {
      case JObject(JField("definition", JString(definitionText)) :: Nil) =>
        userBox.map {
          u =>
            u.definition.is.text.set(definitionText)
            UserEntity.where(_._id eqs u.id).modify(_.definition setTo u.definition.is).updateOne()
            HtmlResource("Changes saved")
        }
    }
  }

  def completeHandler = {
    logger.debug("Attaching complete definition handler")
    bindDataProcessor {
      case JObject(Nil) => userBox.map {
        u =>
          u.definition.is.completed.set(Some(true))
          u.definition.is.estimation.set(None)
          UserEntity.where(_._id eqs u.id).modify(_.definition setTo u.definition.is).updateOne()
          HtmlResource("Changes saved")
      }
    }
  }

  def estimationHandler = {
    logger.debug("Attaching estimate handler")
    bindDataProcessor {
      case JInt(n) => userBox.map {
        u =>
          u.definition.is.estimation.set(Some(n.toInt))
          u.definition.is.completed.set(None)
          UserEntity.where(_._id eqs u.id).modify(_.definition setTo u.definition.is).updateOne()
          HtmlResource("Changes saved")
      }
    }
  }

  protected def userBox = User.currentUser.get ?~ "No current user"
}
