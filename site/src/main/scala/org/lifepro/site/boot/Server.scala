package org.lifepro.site
package boot

import utils.StreamHelpers
import org.mortbay.jetty
import org.mortbay.jetty.webapp.WebAppContext
import java.io.File
import java.util.zip.ZipEntry
import net.liftweb.common._
import java.util.jar.{JarEntry, JarFile}
import net.liftweb.util.Helpers._
import net.liftweb.http.LiftRules
import net.liftweb.common.Full
import scala.Some

/**
 * Start embedded Jetty server with AdsDesk server application deployed on it as root app
 */
object Server extends EntryPoint with LazyLoggable {

  val RUN_DIR = new File(System.getProperty("user.home") + File.separator + ".lifepro")

  val BAD_CLASSPATH_ERROR = "Bad classpath" -> 1
  val JAR_EXTRACTING_ERROR = "Webapp extraction error" -> 2
  val SERVER_DOWN = "Server is down" -> 2

  val DEFAULT_SERVER_PORT = 8080
  val DEFAULT_RUN_MODE = "production"

  def run(args:Array[String]) = {
    LiftBootstrap.loggingSetup.foreach(_())
    logger.info("Starting LifePro server")
    ensureSystemProperty("run.mode", DEFAULT_RUN_MODE)

    installWebapp match {
      case Left(e) => handleError(e)
      case Right(webappRoot) =>
        logger.info(s"Server is starting from  $webappRoot")

        val port = (Box !! System.getProperty("org.lifepro.port")).flatMap(x => tryo(x.toInt)).openOr(DEFAULT_SERVER_PORT)
        val server = new jetty.Server(port)
        val context = new WebAppContext
        context.setServer(server)
        context.setContextPath("/")
        context.setWar(webappRoot)

        server.addHandler(context)

        tryo {
          server.start()
          sys.addShutdownHook(server.stop())
          server.join()
        } match {
          case Failure(_, ebox, _) =>
            ebox.foreach(_.printStackTrace())
            handleError(SERVER_DOWN)
          case _ => 0
        }
    }
  }

  def handleError(e: (String, Int)) = {
    val (error, code) = e
    logger.error(error)
    code
  }

  /**
   * Installs webapp by extracting it from JAR or targeting sources
   */
  def installWebapp: Either[(String, Int), String] = {
    LiftRules.getResource("/webapp").map(_.toString) match {
      case Empty => Left(BAD_CLASSPATH_ERROR)
      case Full(res) if !res.startsWith("jar:") => Right(res)
      case Full(res) => unzipWebApp(res.split("!")(0)) match {
          case Full(_) => Right(RUN_DIR.getPath)
          case Failure(_, Full(e), _) =>
            e.printStackTrace()
            Left(JAR_EXTRACTING_ERROR)
          case _ => Left(JAR_EXTRACTING_ERROR)
        }
    }
  }

  def prepareRunDir = {
    if (RUN_DIR.exists()) {
      def deleteRecursive(file:File) {
        if (file.isDirectory) {
          for {
            child <- file.listFiles()
          } deleteRecursive(child)
        }
        file.delete()
      }
      deleteRecursive(RUN_DIR)
    }
    RUN_DIR.mkdirs()
  }

  def unzipWebApp(jarPath:String) = {
    prepareRunDir

    val jarFile = new JarFile(jarPath.substring(9))
    val entries = jarFile.entries()
    def entriesList = new Iterator[JarEntry] {
      def hasNext = entries.hasMoreElements
      def next() = entries.nextElement
    } .toList

    /*
     * Filter extractor of webapp file jar entry and target file (where to install)
     */
    val WebAppFile = new {
      def unapply(entry:ZipEntry) = {
        val name = entry.getName
        if (!entry.isDirectory && name.startsWith("webapp")) {
          val f = new File(RUN_DIR, name.replace("webapp/", ""))
          Some((entry, f))
        } else None
      }
    }

    val webappEntries = for {
      WebAppFile(entry, target) <- entriesList
    } yield (entry, target)

    import StreamHelpers._
    webappEntries.foldLeft[Box[Unit]](Full(())) {
      case (prevBox, (entry, target)) =>
        prevBox.flatMap(_ => (jarFile.getInputStream(entry) sinkTo target).toOption)
    }
  }

  def ensureSystemProperty(name:String, defaultValue:String) {
    if (sys.props.get(name).isEmpty) {
      System.setProperty(name, defaultValue)
    }
  }
}
