package org.lifepro.site
package boot

import net.liftweb._
import common._
import common.Full
import http._
import http.Html5Properties
import mongodb.{MongoHost, MongoAddress, DefaultMongoIdentifier, MongoDB}

import util.{Mailer, Props}
import utils.MenuHelpers._
import javax.mail.{PasswordAuthentication, Authenticator}
import org.lifepro.site.service.{RecentSection, UniversalRedirect, ResourceService}
import org.lifepro.site.snippet.{Service, User}
import net.liftweb.sitemap.Menu

class LiftBootstrap extends Bootable with LazyLoggable {

  val Main = "Main"

  def boot() {
    LiftRules.addToPackages("org.lifepro.site")

    LiftBootstrap.setupDB
    Logger.setup = LiftBootstrap.loggingSetup

    LiftRules setSiteMap CoolSiteMap(
      Menu.i(Main) / "index" :: Service.siteMenu :: User.siteMenu: _*
    )

    LiftRules.jsArtifacts = net.liftweb.http.js.jquery.JQueryArtifacts

    // Force the request to be UTF-8
    LiftRules.early.append(_.setCharacterEncoding("UTF-8"))

    // Use HTML5 for rendering
    LiftRules.htmlProperties.default.set((r: Req) =>
      new Html5Properties(r.userAgent))

    LiftRules.explicitlyParsedSuffixes += "woff"

    ResourceService.boot()
    Service.boot()
    RecentSection.boot()

    // set default user for development convenience to not re-login after every server restart
    // TODO: think this could be removed after #6 long live sessions will be implemented
    LiftRules.earlyInStateful.append {
      _ =>
        import com.foursquare.rogue.LiftRogue._
        (User.currentUser.get, Props.get("default.user")) match {
          case (Empty, Full(email)) =>
            User.currentUser.set(model.User.where(_.email eqs email).get())
          case _ =>
        }
    }

    UniversalRedirect.append {
      case req =>
        User.currentUser.get.toOption match {
          case Some(_) => Empty
          case _ =>
            for {
              sm <- LiftRules.siteMap
              serviceLoc <- sm.findLoc(Service.Service) if serviceLoc.link.isDefinedAt(req)
              main <- sm.findLoc(Main)
            } yield main.calcDefaultHref
        }
    }

    configMailer
  }

  private def configMailer {

    var isAuth = Props.get("mail.smtp.auth", "false").toBoolean

    Mailer.customProperties = Props.get("mail.smtp.host", "localhost") match {
      case "smtp.gmail.com" =>
        isAuth = true
        Map(
          "mail.smtp.host" -> "smtp.gmail.com",
          "mail.smtp.port" -> "587",
          "mail.smtp.auth" -> "true",
          "mail.smtp.starttls.enable" -> "true"
        )
      case h => Map(
        "mail.smtp.host" -> h,
        "mail.smtp.port" -> Props.get("mail.smtp.port", "25"),
        "mail.smtp.auth" -> isAuth.toString
      )
    }

    if (isAuth) {
      (Props.get("mail.smtp.user"), Props.get("mail.smtp.pass")) match {
        case (Full(username), Full(password)) =>
          Mailer.authenticator = Full(new Authenticator() {
            override def getPasswordAuthentication = new
                PasswordAuthentication(username, password)
          })
        case _ => logger.error("Username/password not supplied for Mailer.")
      }
    }
  }
}
object LiftBootstrap {
  lazy val loggingSetup =
    for {
      url <- Box !! getClass.getResource("/conf/logconfig.xml")
    } yield Logback.withFile(url) _

  def setupDB {
    MongoDB.defineDb(DefaultMongoIdentifier, MongoAddress(MongoHost("127.0.0.1"), "lifepro"))
  }

}

