package org.lifepro.site
package utils

import java.io.{ByteArrayInputStream, FileOutputStream, File, InputStream}
import java.util.jar.JarFile
import java.util.zip.ZipEntry
import scala.io.{Codec, Source}
import java.nio.{CharBuffer}
import scala.util.Try

trait StreamHelpers {
  implicit class InputStreamOps(input: InputStream) {
    /**
     * Sinks input stream into the file
     * @param sink file to sink to
     * @return Full(input) in the case of success Failure if any exceptions were thrown
     */
    def sinkTo(sink: File) = {
      import util.control.Exception
      sink.getParentFile.mkdirs()
      val output = new FileOutputStream(sink)
      Exception.allCatch[Unit].
        andFinally(output.close()).
        withTry {
          input.iterator.map(_.toInt).foreach(output.write _)
          input.close()
        }
    }

    /**
     * Converts input stream into in-memory byte array
     * @return
     */
    def toByteArray = iterator.toArray[Byte]

    /**
     * Converts input stream to a string using implicit codec
     * @param codec
     * @return
     */
    def asString(implicit codec: Codec) = {
      Source.fromInputStream(input).getLines().mkString("\n")
    }

    /**
     * Converts input stream to an iterator over bytes
     * @return
     */
    def iterator:Iterator[Byte] = Iterator.continually(input.read()).takeWhile(-1 !=).map(_.toByte)
  }

  implicit class JarFileOps(jarFile: JarFile) {
    def unzipEntry(entry: ZipEntry, sink: File): Try[Unit] = jarFile.getInputStream(entry) sinkTo sink
  }

  implicit class StringOps(s: String) {
    def toInputStream(implicit codec: Codec) = {
      new ByteArrayInputStream(codec.encoder.encode(CharBuffer.wrap(s)).array())
    }
  }
}

object StreamHelpers extends StreamHelpers
