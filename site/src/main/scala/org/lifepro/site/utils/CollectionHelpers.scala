package org.lifepro.site.utils

import scala.annotation.tailrec

trait CollectionHelpers {
  
  implicit class SeqOps[T](s:Seq[T]) {
    /**
     * Counts equal elements with another sequence
     * @param s2 another sequence
     * @return number of equal elements with this and another sequence
     */
    def countEqualWith(s2:Seq[T]) = {
      @tailrec
      def countRec(s1:Stream[_], s2:Stream[_], acc:Int):Int = (s1, s2) match {
        case ((h1 #:: t1, h2 #:: t2)) if h1 == h2 =>
          countRec(t1, t2, acc + 1)
        case _ =>
          acc
      }
      countRec(s.toStream, s2.toStream, 0)
    }
  }

}

object CollectionHelpers extends CollectionHelpers
