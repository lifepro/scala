package org.lifepro.site.utils

import org.bson.types.ObjectId

/**
 * Different tools for pattern matching
 */
object Unapply {

  object BsonObjectId {
    def unapply(str:String) = if (ObjectId.isValid(str)) Some(new ObjectId(str)) else None
  }

}
