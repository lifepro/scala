package org.lifepro.site
package utils

import scala.xml._
import net.liftweb.common.BoxOrRaw

/**
 * A set of helper methods to work with scala's standard XML library
 */
trait XmlHelpers {

  /**
   * Helper methods for NodeSeq class
   * @param ns target nodeseq
   */
  implicit class NodeSeqOps(ns:NodeSeq) {
    def modifyFirstElem(f: Elem => Elem):NodeSeq =
      ns.zipWithIndex.collect {
        case (e: Elem, 0) => f(e)
        case (ns, _) => ns
      }
  }

  /**
   * Converts a Map[String, String] to the XML attributes
   * @param m a source map to convert
   * @return corresponding XML MetaData object
   */
  implicit def map2attrs(m: Map[String, String]) =
    m.foldLeft(Null: MetaData) {
      case (md, (key, value)) =>
        Attribute(key, Text(value), md)
    }

  /**
   * Converts a (String, String) pair to the XML attribute
   * @param pair a source pair of values
   * @return corresponding XML MetaData object
   */
  implicit def pair2attrs(pair: (String, String)) =
    Map(pair) : MetaData

  /**
   * Appends a value to an attribute of the elem, also could append just attribute itself if text is set to None
   * @param AttrName the attribute name
   * @param separator a separator char sequence
   * @param e a source elem which attribute appended to
   * @param text the text to append to attribute (None could be used to just append the attribute without the value)
   * @return metadata consisting of a single attribute either modified or created if not found
   */
  def appendToAttr(AttrName:String, separator:String = "")(e:Elem, text:BoxOrRaw[String]) = {
    def appendRec(attrs:MetaData):MetaData = attrs match {
      case Null =>
        new UnprefixedAttribute(AttrName, text.map(Text(_)).openOr(Nil), Null)
      case attr @ UnprefixedAttribute(key, value, next) if key == AttrName =>
        text.map { t =>
          val attrText = value.text.trim
          val optionalSeparator = if (attrText.size == 0 || attrText.endsWith(separator)) "" else separator
          new UnprefixedAttribute(key, Text(attrText + optionalSeparator + t), Null)
        } .getOrElse(Null)
      case m => appendRec(m.next)
    }
    appendRec(e.attributes)
  }

}

object XmlHelpers extends XmlHelpers
