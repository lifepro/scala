package org.lifepro.site
package utils

import net.liftweb.json._
import net.liftweb.util.Html5
import scala.xml.NodeSeq
import net.liftweb.http.S
import net.liftweb.common.LazyLoggable

/**
 * Different JSON tools
 */
trait JsonHelpers extends LazyLoggable with MenuHelpers {

  import net.liftweb.json.JsonDSL._
  /**
   * Converts almost any value to JValue
   * @param v the value of any type
   * @return corresponding JValue or JNothing if type isn't supported
   */
  implicit def any2jv(v: Any)(implicit formats:Formats = DefaultFormats): JValue = v match {
    case v if formats.customSerializer.isDefinedAt(v) =>
      formats.customSerializer.apply(v)
    case v: JValue => v
    case int: Int => int2jvalue(int)
    case long: Long => long2jvalue(long)
    case bigInt: BigInt => bigint2jvalue(bigInt)
    case double: Double => double2jvalue(double)
    case float: Float => float2jvalue(float)
    case bigDecimal: BigDecimal => bigdecimal2jvalue(bigDecimal)
    case boolean: Boolean => boolean2jvalue(boolean)
    case string: String => string2jvalue(string)
    case map: Map[_, _] => map2jvalue(map.map {
      case (k, v) => k.toString -> v
    }.toMap)
    case ns: NodeSeq => string2jvalue(ns.map(Html5.toString).mkString)
    case (field: String, value) => pair2jvalue(field, value)
    case _ => JNothing
  }

  case class HtmlResource(str: String) {
    lazy val toJValue = JString {
      val localized = S.loc(str)
      localized.map(ns => ns.map(Html5.toString).mkString).openOr(str)
    }
  }

  object HtmlResource {
    lazy val serializer: Serializer[HtmlResource] = new CustomSerializer[HtmlResource](
      f => ( {
        case _ if false => null
      }, {
        case res: HtmlResource => res.toJValue
      })
    )
  }

  implicit class JObjectOps(jObj:JObject) {
    /**
     * Drops everything from the object except the path specified
     * @param path field names to specifying interested path in the object
     * @return copy of object with non-path fields dropped
     */
    def reduce(path: List[String]):JObject = {
      path match {
        case head :: rest =>
          JObject(jObj.obj.collect {
            case JField(name, value @ JObject(_)) if name == head =>
              JField(name, value.reduce(rest))
            case JField(name, value) if name == head && rest.isEmpty =>
              JField(name, value)
          })
        case Nil =>
          jObj
      }
    }
    def reduce(path: String*):JObject = reduce(path.toList)
  }

}

object JsonHelpers extends JsonHelpers
