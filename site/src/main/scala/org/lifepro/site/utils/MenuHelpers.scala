package org.lifepro.site
package utils

import net.liftweb.sitemap._
import net.liftweb.http.{LiftRules, Req, S}
import net.liftweb.common.{Box, Empty, LazyLoggable}
import net.liftweb.sitemap.Menu.{ParamsMenuable, ParamMenuable, BaseMenuable, Menuable}
import net.liftweb.sitemap.Loc.Test
import org.lifepro.site.service.ResourceService

/**
 * Defines helper methods for work with lift's site menu
 */
trait MenuHelpers extends LazyLoggable {

  def linkTo[T](name: String, value: Option[T] = None): Box[String] =
    for {
      sm <- LiftRules.siteMap
      loc <- sm findLoc name
      link <- value.flatMap { v =>
        loc match {
          case loc: Loc[T] => Some(loc.calcHref(v))
          case _ => None
        }
      } orElse loc.defaultLink
    } yield link

  implicit class ReqOps(req: Req) {
    def asReferer =
      for {
        referer <- req.header("Referer")
      } yield {
        val uri = new java.net.URI(referer)
        req.withNewPath(Req.parsePath(uri.getRawPath))
      }

    def asPrevious =
      for {
        previous <- req.header("Previous-Page")
      } yield req.withNewPath(Req.parsePath(previous))

    def original = Req(req.request, Nil, 0)
  }

  object CoolSiteMap {
    def apply(menus: ConvertableToMenu*) = SiteMap(menus: _*) //new HonorRefererSiteMap(menus: _*)
  }

  implicit class LocOps[T](loc: Loc[T]) {
    /**
     * Advanced method to get the location value
     * @return current value of the Loc[T] or the value parsed from the referrer header
     *         if current request relates to the AJAX template
     */
    def referredValue =
      loc.requestValue.doWith(Empty) {
        (for {
          req <- S.request
          rewritePF <- loc.rewritePF
          _ = Req(req, rewritePF :: Nil)
        } yield loc.currentValue or {
            for {
              asReferer <- req.asReferer if req.path.suffix == ResourceService.PARTIAL_PAGE_SUFFIX || req.ajax_?
              _ = Req(asReferer, rewritePF :: Nil)
              v <- loc.currentValue
            } yield v
          }).flatMap(identity)
      } or loc.currentValue

    /**
     * The missing method for non param locations
     * @return current path for the location
     */
    def defaultLink = loc.createDefaultLink.map(_.text)
  }

  implicit class BaseMenuableOps(m: BaseMenuable with ConvertableToMenu) {
    def withChildren(submenus: BaseMenuable*) = {
      def addBasePath: PartialFunction[Any, ConvertableToMenu] = {
        case sm: Menuable =>
          new Menuable(
            sm.name,
            sm.linkText,
            m.path ::: sm.path,
            sm.headMatch,
            sm.params,
            sm.submenus.map(addBasePath)
          )
        case pm: ParamMenuable[_] =>
          new ParamMenuable(
            pm.name,
            pm.linkText,
            pm.parser,
            pm.encoder,
            m.path ::: pm.path,
            pm.headMatch,
            pm.params,
            pm.submenus.map(addBasePath)
          )
        case v: ConvertableToMenu => v
      }
      val hideParam = Test(_ => false)
      val patchedSubmenus = submenus.toList.map(addBasePath)
      m match {
        case m: Menuable => m.submenus(patchedSubmenus) >> hideParam
        case pm: ParamsMenuable[_] => pm.submenus(patchedSubmenus) >> hideParam
        case v => v
      }
    }
  }
}

object MenuHelpers extends MenuHelpers
