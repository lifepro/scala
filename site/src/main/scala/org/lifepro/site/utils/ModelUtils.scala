package org.lifepro.site
package utils

import com.foursquare.index.MongoIndex
import net.liftweb.json.JsonAST.JObject
import net.liftweb.json.JValue

/**
 * A trait to simplify use of lift mongo record ensureIndex method with rogue indexes syntax
 * to define indexes for model classes
 */
trait ModelUtils extends JsonHelpers {
  implicit def index2jo[T](idx:MongoIndex[T]) = (idx.asListMap : JValue).asInstanceOf[JObject]
}
object ModelUtils extends ModelUtils
