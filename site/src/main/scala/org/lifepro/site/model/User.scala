package org.lifepro.site
package model

import net.liftweb.mongodb.record.{BsonMetaRecord, BsonRecord, MongoMetaRecord, MongoRecord}

import net.liftweb.record.field.{OptionalIntField, OptionalBooleanField, StringField}
import com.foursquare.index.{Asc}
import com.foursquare.rogue.ObjectIdKey
import com.foursquare.rogue.LiftRogue._
import utils.ModelUtils
import net.liftweb.mongodb.record.field.{BsonRecordField, DateField}


trait PasswordMixin[T <: PasswordMixin[T]] extends MongoRecord[T] {
  self: T =>
  object password extends StringField(self, 32)
}

trait EmailWithId[T <: EmailWithId[T]] extends MongoRecord[T] with ObjectIdKey[T] {
  self: T =>
  object email extends StringField(self, 50)
}

trait EmailWithIdMeta[T <: EmailWithId[T]] extends MongoMetaRecord[T] with ModelUtils {
  self: T =>

  ensureIndex(this.index(_.email, Asc), true)
}

trait UserRegistration[T <: UserRegistration[T]] extends EmailWithId[T] with PasswordMixin[T] {
  self: T =>
}

class Definition private() extends BsonRecord[Definition] {
  def meta = Definition
  object text extends StringField(this, 500)
  object estimation extends OptionalIntField(this)
  object completed extends OptionalBooleanField(this)
  object confirmed extends OptionalBooleanField(this)
}

object Definition extends Definition with BsonMetaRecord[Definition] {
}

class User private() extends UserRegistration[User] {
  object firstName extends StringField(this, 50)
  object lastName extends StringField(this, 50)
  object definition extends BsonRecordField(this, Definition)
  object lastLoggedIn extends DateField(this)
  
  def definitionMaterial = Material.createRecord.
    body(definition.is.text.is).
    bodyConfirmed(definition.is.confirmed.is).
    definition(Some(true))

  def meta = User
}


object User extends User with EmailWithIdMeta[User] {
}

class Registration private() extends UserRegistration[Registration] with ObjectIdKey[Registration] {
  def meta = Registration
}

object Registration extends Registration with EmailWithIdMeta[Registration] {
}

class PasswordResetRequest private() extends EmailWithId[PasswordResetRequest] {
  def meta = PasswordResetRequest
}

object PasswordResetRequest extends PasswordResetRequest with EmailWithIdMeta[PasswordResetRequest] {
}
