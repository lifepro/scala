package org.lifepro.site
package model

import net.liftweb.mongodb.record.{BsonMetaRecord, MongoRecord, BsonRecord, MongoMetaRecord}
import net.liftweb.record.field._
import net.liftweb.mongodb.record.field.{ObjectIdRefField, BsonRecordField, DateField}
import net.liftweb.json.JsonAST.{JArray, JString, JNothing, JObject}
import net.liftweb.json.{JValue, JsonAST, JsonDSL}
import org.lifepro.site.snippet.{Definition => DefinitionSnippet, ScaleUp}
import com.foursquare.rogue.ObjectIdKey
import com.foursquare.rogue.LiftRogue._
import java.util.Date
import org.bson.types.ObjectId
import net.liftweb.record.OptionalTypedField
import org.lifepro.site.utils.JsonHelpers

class Material extends ObjectIdKey[Material] with MongoRecord[Material] with JsonDSL {
  def meta = Material

  object body extends StringField(this, 500)
  object owner extends ObjectIdRefField(this, User)
  object bodyConfirmed extends BooleanField(this)
  object modifiedTs extends DateField(this) {
    override def asJValue: JsonAST.JValue = get.getTime
  }
  object conclusion extends OptionalStringField(this, 500)
  object conclusionConfirmed extends BooleanField(this)
  object removed extends BooleanField(this)
  object tt extends BsonRecordField(this, TT)
  object gsrp extends BsonRecordField(this, GSRP)
  object definition extends OptionalBooleanField(this)
  object parent extends OptionalStringField(this, 200)
  object previousBody extends OptionalStringField(this, 500)

  def refresh = {
    modifiedTs(new Date)
    this
  }

  def parentMaterial =
    for {
      id <- parent.get
      material <- Material.where(_._id eqs ObjectId.massageToObjectId(id)).get
    } yield material

  def childMaterial(finished:Boolean) =
    JArray(
      for {
        m <- Material.where(_.parent eqs id.toString).
          and(_.removed eqs false).
          and(_.conclusion.exists(finished)).
          fetch()
      } yield JString(ScaleUp.location.calcHref(m))
    )

  override def asJValue: JObject = super.asJValue ~
    ("workoutLink" -> workoutLink) ~ ("gsrpLink" -> gsrpLink) ~ ("ttLink" -> ttLink) ~
    ("childLinks" -> (
      ("blocking" -> childMaterial(false)) ~
      ("resolved" -> childMaterial(true))
    )) ~
    ("parentLink" -> {
      for {
        id <- parent.get
        m <-  Material.where(_._id eqs ObjectId.massageToObjectId(id)).get()
      } yield ScaleUp.location.calcHref(m)
    } .map(JString(_)).getOrElse(JNothing))

  def workoutLink: JValue = (isDefinition, isTT, isGSRP) match {
    case (true, _, _) => DefinitionSnippet.definitionWorkoutUrl
    case (_, true, _) => ttLink
    case (_, _, true) => gsrpLink
    case _ => JNothing
  }

  def gsrpLink = ScaleUp.location.calcHref(this)

  def ttLink = snippet.TT.location.calcHref(this)

  def isTT = tt.get.started.get.map(identity).getOrElse(false)

  def isGSRP = gsrp.get.started.get.map(identity).getOrElse(false)

  def isDefinition = definition.get.map(identity).getOrElse(false)
}

trait Process[T <: Process[T]] extends BsonRecord[T] {
  self: T =>

  object started extends OptionalBooleanField(self)
}

class TT private() extends Process[TT] {
  def meta = TT

  object myFeelings extends OptionalStringField(this, 200)
  object myWishes extends OptionalStringField(this, 200)
  object mineConfirmed extends OptionalBooleanField(this)
  object hisFeelings extends OptionalStringField(this, 200)
  object hisWishes extends OptionalStringField(this, 200)
  object hisConfirmed extends OptionalBooleanField(this)
}

class GSRP private() extends Process[GSRP] {
  def meta = GSRP

  object intro extends OptionalStringField(this, 200) {
    override def asJValue = get.map(v => JsonHelpers.HtmlResource(v).toJValue).getOrElse(JNothing)
  }
  object feelings extends OptionalStringField(this, 200)
  object wishes extends OptionalStringField(this, 200)
  object estimation extends OptionalIntField(this)
  object mostUnpleasant extends OptionalStringField(this, 200)
  object mostUnpleasantConfirmed extends OptionalBooleanField(this)
  object whatToChange extends OptionalStringField(this, 200)
  object whatToChangeConfirmed extends OptionalBooleanField(this)

  def clear() = {
    Seq[OptionalTypedField[_]](
      estimation,
      feelings,
      wishes,
      mostUnpleasant,
      mostUnpleasantConfirmed,
      whatToChange,
      whatToChangeConfirmed
    ) map (_ set None)
    this
  }
}

object Material extends Material with MongoMetaRecord[Material] {
}

object TT extends TT with BsonMetaRecord[TT] {

}

object GSRP extends GSRP with BsonMetaRecord[GSRP] {
}
