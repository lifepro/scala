package org.lifepro.site
package service

import net.liftweb.http.{S, Req}
import net.liftweb.common.{Box}
import scala.collection.mutable.ListBuffer
import scala.xml.{NodeSeq}
import net.liftweb.util.ListHelpers
import org.lifepro.site.utils.MenuHelpers

/**
 * Implements logic where redirection can be passed in response via HTTP (302)
 * or via the injecting a snippet into the template body, to make client side URL rewrite
 */
object UniversalRedirect extends MenuHelpers {

  import utils.XmlHelpers._

  type RedirectPF = PartialFunction[Req, Box[String]]

  private val logic = ListBuffer.empty[RedirectPF]

  def append(pf:RedirectPF) {
    logic += pf
  }

  def asSnippetFor(req:Req):NodeSeq => NodeSeq = { ns =>
    val redirectUrl = ListHelpers.first(for {
      redirect <- logic.toList if redirect.isDefinedAt(req)
    } yield redirect)(_(req))

    redirectUrl.map { url =>
      if (req.original.path.suffix == ResourceService.PARTIAL_PAGE_SUFFIX) {
        ns.modifyFirstElem { e =>
          e % appendToAttr("ng-init", ";")(e, s"redirect('$url')")
        }
      } else {
        S.redirectTo(url)
        ns
      }
    } .openOr(ns)
  }

}
