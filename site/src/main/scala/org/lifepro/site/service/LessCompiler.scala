package org.lifepro.site
package service

import net.liftweb.common.LazyLoggable
import net.liftweb.util.{ControlHelpers, TimeHelpers, Props}
import java.net.URL
import lesst.StyleSheet

/**
 * The root for CSS resources compilation facilities.
 * Manages the cache and determines the need of recompilation.
 */
trait LessCompiler extends TimeHelpers with ControlHelpers with LazyLoggable {

  protected var cache = Map.empty[URL, CachedResult]

  class CachedResult(sourceURL: URL, result: StyleSheet) {

    import java.io.File

    protected val compilationTime = now.getTime
    protected lazy val mainFile = new File(sourceURL.toURI)
    protected lazy val parentDir = mainFile.getParentFile
    protected lazy val dependencies = result.imports.map(p => new File(parentDir, p))

    val source = result.src

    def isActual = (mainFile :: dependencies).forall(_.lastModified() < compilationTime)
  }

  def compileLess = lesst.Compile().minify(!Props.devMode)

  def cacheResult(url: URL, result: StyleSheet) = cache += url -> new CachedResult(url, result)

  /**
   * Compiles given URL
   * @param url the URL of the root LESS file
   */
  def compile(url: URL): String = {
    val timeStart = millis
    cache.get(url) match {
      case Some(cached) if cached.isActual =>
        logger.debug(s"Returning compiled $url from the cache")
        cached.source
      case _ =>
        logger.debug(s"Compiling LESS source $url")
        compileLess(url) match {
          case Left(error) =>
            logger.error(s"Failed to compile LESS source $url", error)
            ""
          case Right(result) =>
            val imports = result.imports
            val timeTaken = millis - timeStart
            logger.debug(s"LESS source $url compiled successfully in ${timeTaken}ms. Imports list: $imports")
            cacheResult(url, result)
            result.src
        }
    }
  }
}

object LessCompiler extends LessCompiler

