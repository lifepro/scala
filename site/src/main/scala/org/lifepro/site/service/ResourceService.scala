package org.lifepro.site
package service

import net.liftweb.http._
import net.liftweb.common._
import script.JsCompiler
import utils.StreamHelpers._
import net.liftweb.util._
import scala.xml.{NodeSeq, Elem}
import net.liftweb.builtin.snippet.Surround
import org.lifepro.site.utils.{MenuHelpers, XmlHelpers}
import eu.henkelmann.actuarius.ActuariusTransformer
import scala.io.{Source, Codec}
import java.util.ResourceBundle
import java.util
import java.net.URL
import scala.Some
import net.liftweb.http.ParsePath
import net.liftweb.http.InMemoryResponse
import net.liftweb.common.Full
import net.liftweb.http.RewriteRequest

/**
 * Serves pages, partial pages, css (compiled from less), js (from coffee script sources)
 */
object ResourceService extends Loggable {

  val PARTIAL_PAGE_SUFFIX = "tmpl"

  val CSS_RESOURCES_ROOT: String = "css-hidden"
  val SCRIPT_RESOURCES_ROOT: String = "scripts-hidden"
  val TEMPLATE_RESOURCES_ROOT: String = "templates-hidden"
  val SITE_PAGES_ROOT: String = "site-pages-hidden"
  val CONTENT_ROOT: String = "content-hidden"

  val hiddenFolders = Seq(
    CSS_RESOURCES_ROOT,
    SCRIPT_RESOURCES_ROOT,
    TEMPLATE_RESOURCES_ROOT,
    SITE_PAGES_ROOT,
    CONTENT_ROOT
  )

  lazy val SURROUND_CONTENT_TAGS =
    "main" :: "section" :: "subsection" :: Nil

  class Resource(val url: URL) {

    lazy val asString = url.openStream.asString

    lazy val file = new java.io.File(url.toURI)
  }

  object Resource {
    def box(path: List[String], suffix: String) = {
      lazy val resourcePath = path.mkString("/", "/", ".") + suffix
      LiftRules.getResource(resourcePath).map(new Resource(_))
    }
  }

  def boot(): Unit = {
    // close access to hidden resources folders
    LiftRules.statelessRewrite.append {
      case RewriteRequest(ParsePath(p :: _, _, _, _), GetRequest, _)
        if hiddenFolders.exists(_ == p) =>
        RewriteResponse("bad-request.html" :: Nil, true)
    }

    // no need for state while generating JS and CSS
    LiftRules.statelessDispatch.
      append(redirection).
      append(Serve.script).
      append(Serve.css)

    // templates and pages need session
    LiftRules.explicitlyParsedSuffixes += PARTIAL_PAGE_SUFFIX
    LiftRules.templateSuffixes ::= "md"
    LiftRules.dispatch.append(Serve.page)

    LiftRules.resourceForCurrentLoc.default.set(
      () => Build.requestContentBundle.toList ::: DefaultRoutines.resourceForCurrentReq()
    )
  }

  lazy val redirection = {
    object IndexPath {
      def unapply(p: List[String]) = if (p.length > 0 && p.last == "index") Some(p.dropRight(1)) else None
    }
    implicit def responsePromoter(r: LiftResponse): () => Box[LiftResponse] = () => Box !! r
    val redirectPages: LiftRules.DispatchPF = {
      case r@Req(IndexPath(path), "" | "html", GetRequest) if !r.path.endSlash =>
        RedirectResponse(path match {
          case Nil => "/"
          case p => p.mkString("/", "/", "/")
        })
      case Req(path, "html", GetRequest) =>
        RedirectResponse(path.mkString("/", "/", ""))
    }
    redirectPages
  }

  object Build {
    class CoffeeCompiler extends service.script.CoffeeCompiler("coffee-script.js")
    def coffee(path: List[String]) =
      for {
        coffeeSource <- Resource.box(path, "coffee").map(_.asString)
        jsSource <- {
          val compiler = new CoffeeCompiler
          val result = compiler.compile(coffeeSource, false)
          result.left.toOption.foreach(logger.error(_))
          result.right.toOption
        }
      } yield jsSource

    def script(path: List[String]) =
      for {
        jsSource <- coffee(path) or Resource.box(path, "js").map(_.asString)
        jsCompiled = if (Props.devMode) jsSource else JsCompiler.compile(jsSource)
      } yield jsCompiled

    def css(path: List[String]) =
      for {
        sourceUrl <- Resource.box(path, "less").map(_.url)
        cssSource = LessCompiler.compile(sourceUrl)
      } yield cssSource

    object MarkdownFile {
      val suffix = ".md"

      def unapply(s: String) = if (s.endsWith(suffix)) Some(s.substring(0, s.length - suffix.length)) else None
    }

    def requestContentBundle: Box[ResourceBundle] =
      for {
        session <- S.session
        req <- S.request
        path = req.path.partPath
        htmlProps = session.requestHtmlProperties
        contentPath = CONTENT_ROOT :: path.dropRight(1)
        keys <- LiftRules.loadResourceAsString(contentPath.mkString("/", "/", "")).
          map(Source.fromString).
          map(_.getLines.collect {
          case MarkdownFile(name) => name
        })
      } yield {
        import scala.collection.JavaConversions._
        new ResourceBundle() {
          private def templateFrom(key:String) =
            Templates(contentPath ::: key :: Nil)
          def handleGetObject(key: String): AnyRef =
            htmlProps.doWith {
              val transformer = new ActuariusTransformer
              htmlProps.is.setHtmlParser {
                is =>
                  import utils.StreamHelpers._
                  implicit val codec = Codec.UTF8
                  val htmlString = transformer(is.asString)
                  val htmlStream = s"<div>$htmlString</div>".toInputStream
                  Html5.parse(htmlStream) match {
                    case Full(elem: Elem) => Full(NodeSeq.fromSeq(elem.child))
                    case b => b
                  }
              }
            }(templateFrom(key) or templateFrom(key.toLowerCase.replaceAll(" ", "_")) ?~ "Seems like .md wasn't added to templates suffix").
              openOrThrowException(".md should be added to templates suffix by ResourceService")

          def getKeys: util.Enumeration[String] = keys
        }
      }
  }

  object Serve {
    lazy val script: LiftRules.DispatchPF = {
      case Req(list, "js", GetRequest) if list.indexOf("ajax_request") == -1 && list.indexOf("js") == 0 =>
        js {
          val path = list.tail
          debugReq("JS", path)
          Resource.box(list, "js").map(_.asString) or Build.script(SCRIPT_RESOURCES_ROOT :: path)
        }
    }

    lazy val css: LiftRules.DispatchPF = {
      case Req(list, "css", GetRequest) if list.indexOf("css") == 0 =>
        css {
          val path = list.tail
          debugReq("CSS", path)
          Resource.box(list, "css").map(_.asString) or Build.css(CSS_RESOURCES_ROOT :: path)
        }
    }

    lazy val page: LiftRules.DispatchPF = {
      case req@Req(reqPath, suffix, GetRequest) if suffix == "" || suffix == PARTIAL_PAGE_SUFFIX =>
        () =>
          import MenuHelpers.ReqOps
          lazy val original = req.original
          lazy val isPartial = original.path.suffix == PARTIAL_PAGE_SUFFIX
          for {
            session <- S.session
            template = Templates(TEMPLATE_RESOURCES_ROOT :: reqPath).filter(_ => isPartial)
            pagePath = (original.asPrevious openOr original).path.partPath
            nodes <- template or {
              val depth = Full(reqPath.size).filter(_ => !isPartial) openOr {
                import utils.CollectionHelpers._
                Full(reqPath.size - reqPath.countEqualWith(pagePath) - 1).filter(_ > 0).openOr(0)
              }
              (Box(reqPath.lastOption).filter(_ == RecentSection.RECENT_PATH_TRIGGER).map { _ =>
                <div></div>
              } or Templates(SITE_PAGES_ROOT :: reqPath)).map {
                UniversalRedirect.asSnippetFor(req)
              } map {
                calcSurround(reqPath, depth)
              } map { ns =>
                if (isPartial) {
                  // generate new title for partial page
                  val dynTitle = session.processSurroundAndInclude("dynamic-title",
                    <title class="hidden" dynamic-title="" data-lift="Title.joined">SiteName</title>
                  )
                  <div>{ns ++ dynTitle}</div>
                } else ns
              }
            }
            _ = debugReq(if (isPartial) "Partial page" else "Page", reqPath)
            response <- session.processTemplate(
              Full(nodes),
              req, req.path, 200)
          } yield response
    }

    def calcSurround(path:List[String], depth:Int):NodeSeq => NodeSeq = {
      lazy val surroundsList = {
        val pathVector = path.toVector
        for {
          n <- (1 to path.size).toIterator if n <= depth
          initPath = pathVector.dropRight(n)
          tag <- SURROUND_CONTENT_TAGS.drop(initPath.size).headOption
          surroundTemplate = (initPath :+ "default").mkString("/")
        } yield { nodes:NodeSeq =>
          import XmlHelpers._
          S.withAttrs(Map("with" -> surroundTemplate, "at" -> tag)) {
            Surround.render(nodes)
          }
        }
      }
      surroundsList.reduceLeftOption(_ andThen _).getOrElse(identity)
    }

    def withMime[T](mimeType: String)(resp: => Box[T]): () => Box[LiftResponse] =
      () => for {
        r <- resp
      } yield stringResponse(r.toString, Option(mimeType))

    def html[T <: NodeSeq](resp: => Box[T]): () => Box[LiftResponse] =
    // using html5 properties correctly
      () => for {
        nodes <- resp
        request <- S.request
      } yield LiftRules.convertResponse(nodes, Nil, Nil, request)

    def css[T](resp: => Box[T]) = withMime("text/css")(resp)

    def js[T](resp: => Box[T]) = withMime("application/x-javascript")(resp)

    implicit class TraversableOps[T](t: TraversableOnce[T]) {
      def firstOf[U](f: T => Box[U]) = ListHelpers.first(t.toSeq)(f)
    }

    protected def stringResponse(string: String, contentType: Option[String]) =
      InMemoryResponse(
        string.getBytes("UTF-8"),
        contentType.map(v => ("Content-Type" -> v) :: Nil).getOrElse(Nil),
        Nil,
        200
      )

    protected def debugReq(kind: String, path: List[String]) =
      logger.debug(s"${kind} request: " + path.mkString("/"))
  }


}
