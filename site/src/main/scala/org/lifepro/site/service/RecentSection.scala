package org.lifepro.site.service

import net.liftweb.http._
import net.liftweb.common.{Box, LazyLoggable}
import net.liftweb.sitemap.Loc.AnyLocParam
import net.liftweb.common.Full
import net.liftweb.sitemap.{Loc, Menu}

/**
 * This singleton object manages recent subsection visited by user to let
 * main navigation decide which subsection to redirect user to
 */
object RecentSection extends LazyLoggable {

  val RECENT_PATH_TRIGGER = "_recent"

  case class DefaultRecentSection(name:String) extends AnyLocParam

  private object recentLocations extends SessionVar[Map[List[String], String]](Map.empty)

  def boot() {
    LiftRules.earlyInStateful.append { reqBox =>
      for {
        req <- reqBox if req.get_?
        Req(path, suffix, _) = req
        loc <- req.location if (suffix == ResourceService.PARTIAL_PAGE_SUFFIX || suffix == "") &&
          path.lastOption.filter(_ == RECENT_PATH_TRIGGER).isEmpty
      } visit {
        loc.currentValue.map { v =>
          loc.asInstanceOf[Loc[Any]].link.createPath(v)
        } .openOr(loc.calcDefaultHref)
      }
    }
  }

  def visit(path:String) {
    val p = path.split("/").toList.drop(1)
    for {
      i <- 1 until p.size
    } recentLocations.update(_ + (p.take(i) -> path))
  }

  UniversalRedirect.append {
    case req if req.get_? && req.path.partPath.lastOption.filter(_ == RECENT_PATH_TRIGGER).isDefined =>

      def mathchedLocs(menus:Seq[Menu], acc:Seq[Loc[_]] = Seq.empty):Seq[Loc[_]] =
        menus.toList match {
          case h :: rest => mathchedLocs(rest,
            acc ++ (if (h.loc.link.isDefinedAt(req)) Some(h.loc) else None) ++
            mathchedLocs(h.kids)
            )
          case _ => acc
        }

      val foundSection = for {
        href <- {
          val path = req.path.partPath match {
            case p => p.take(p.size - 1)
          }
          List[Box[String]](
            recentLocations.get.get(path),
            for {
              siteMap <- LiftRules.siteMap
              loc <- mathchedLocs(siteMap.kids).sortBy(_.link.uriList.size).lastOption
              defaultLoc <- {
                (for {
                  defaultSection <- loc.allParams.
                    find(_.isInstanceOf[DefaultRecentSection]).
                    map(_.asInstanceOf[DefaultRecentSection])
                  defaultLoc <- siteMap.findLoc(defaultSection.name)
                } yield defaultLoc) orElse Some(loc)
              }
            } yield defaultLoc.calcDefaultHref
          ).reduceLeft(_ or _)
        }
      } yield href

      foundSection orElse Full("/")
  }

}
