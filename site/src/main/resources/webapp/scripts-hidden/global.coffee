###
  Global script file. Included with every page.
  Adds site navigation control.
###

LOAD_STARTED = 'load_started'
LOAD_FINISHED = 'load_finished'

if not Array.prototype.every
  Array.prototype.every = (f) ->
    (x for x in @ when f(x)).length == @length

module = angular.module 'lifepro', [], ['$locationProvider', (locationProvider) ->
  locationProvider.html5Mode true
  locationProvider.hashPrefix '!'
]

module.run ['$rootScope', '$injector', '$log', '$location', '$window', (rootScope, injector, log, location, window) ->
  log.info "Initializing lifepro module"

  if navigator.userAgent.indexOf("MSIE") > -1
    document.body.classList.add("ie");

  rootScope.onSectionLoaded = ->
    rootScope.sectionLoaded = true
    rootScope.$broadcast 'sectionLoaded'

  angular.merge = (dst, src) ->
    for k, v of src
      if angular.isObject(dst[k]) && angular.isObject(src[k])
        angular.merge dst[k], src[k]
      else
        dst[k] = src[k]
    dst

  rootScope.redirect = (to) ->
    location.url(to).replace()
    window.scrollTo(0, 0)
  rootScope.setTitle = (title) ->
    window.title = title

  emptyToIndex = (v) -> if v is "" then "index" else v
  location.canonize = (p) ->
    p.split("?")[0].split('/').slice(3).map emptyToIndex

  rootScope.location = location
  rootScope.sectionLoaded = true
]

###
  A directive to listen the location path and include coressponding section if path changes
  @param level the level of the section (e.g. /page1 has level 1, /sec1/page2 has level 2),
  the section will be upaded only in the case if path changed exacly at the level of the section
  otherwise path update ignored for this section
###
module.directive 'sectionInclude', ['$log', '$compile', '$location', '$http', (log, compile, location, http) ->
  (scope, element, attrs) ->
    contentScope = null
    watchLevels = attrs.sectionInclude.split(',').map (v) ->
      parseInt(v)
    .filter (v) ->
      !isNaN(v)
    if (watchLevels.length > 0)
      stopWatch = scope.$root.$on '$locationChangeSuccess', (e, newPath, oldPath) ->
        return if oldPath == newPath
        oldPath = location.canonize oldPath
        newPath = location.canonize newPath

        # the level in the path at which path changed
        changedLevel = (->
          i = 0
          while i < newPath.length && i < oldPath.length && oldPath[i] == newPath[i]
            i += 1
          i + 1
        )()
        parentLevelChanged = watchLevels.every (level) -> level > changedLevel
        if watchLevels.indexOf(changedLevel) != -1 && changedLevel <= newPath.length
          query = location.url().split("?")
          query = if query.length > 1 then query[1] else ""

          url = '/' + newPath.join('/') + '.tmpl' + if (query) then "?" + query else ""
          http.get(url, headers: 'Previous-Page': '/' + oldPath.join('/')).success (html) ->
            contentScope.$destroy() if contentScope
            contentScope = scope.$new()
            element.html compile(html) contentScope
            scope.onSectionLoaded()
        else if parentLevelChanged
          stopWatch()
]


###
  A controller mixed in by org.lifepro.site.snippet.AjaxBinding and working in pair with it
###
module.controller 'ServerRoundtrip', ['$scope', '$log', '$rootScope', (scope, log, root) ->
  pattern = /angular.noop\((.*)\)/
  scope.roundtripInProgress = false
  scope.sendData = (data, resultHandler, errorHandler) ->
    scope.$emit LOAD_STARTED
    scope.roundtripInProgress = true
    scope.roundtripFailed = false

    resultHandler0 = (result) ->
      result = result.match(pattern)[1]
      scope.$emit(LOAD_FINISHED)
      root.$apply ->
        root.autoSaveDisabled = true
      scope.$apply ->
        scope.roundtripInProgress = false
        result = JSON.parse(result)
        if result.result?.redirectTo?
          root.redirect result.result.redirectTo
        else
          resultHandler(result) if resultHandler?
          honorContextUpdate(result)
      root.$apply ->
        delete root.autoSaveDisabled

    honorContextUpdate = (result) ->
      if result.result?.context?
        angular.merge scope.context, result.result.context

    errorHandler0 = ->
      scope.$emit LOAD_FINISHED
      scope.$apply ->
        scope.roundtripInProgress = false
        scope.roundtripFailed = true
        errorHandler arguments... if errorHandler?

    liftAjax.lift_ajaxHandler(
      scope.funcId + "=" + encodeURIComponent(JSON.stringify(data)),
      resultHandler0,
      errorHandler0
    )

]

###
  A directive to link to ajax spinner element for scope server roundtrip handler
###
module.directive 'roundtripSpinner', ['$log', (log) ->
  (scope, element, attrs) ->
    scope.$watch 'roundtripInProgress', (inProgress) ->
      if inProgress
        element.show 'fast', ->
          # a dummy fix for jQuery show/hide animation bug when switching too fast
          element.hide() if !scope.roundtripInProgress
      else element.hide()
]

###
  Personal HTML bind derictive
  Also supports flash mode - given a number of milliseconds shows an element for that time on scope var change
###
module.directive 'bindHtml', ['$compile', '$log', (compile, log) ->
  (scope, element, attrs) ->
    varName = attrs.bindHtml
    timeout = parseInt(attrs.flash)
    flash = !isNaN(timeout)
    timeoutId = -1
    initialHtml = element.html()
    defaultHidden = initialHtml.length == 0

    scope.$watch varName, (html) ->
      if html?
        compile(element.html(html).children())(scope)

        if defaultHidden || flash
          element.fadeIn('fast')

        if flash
          clearInterval(timeoutId)
          callback = () ->
            element.fadeOut 'fast', () ->
              scope.$apply () ->
                scope[varName] = null
          timeoutId = setTimeout(callback, timeout)
      else
        element.html(initialHtml)
        if (defaultHidden)
          element.hide()
]

module.directive 'loginWindow', ['$rootScope', (rootScope) ->
  (scope, element, attrs) ->
    element.addClass('login-window')
    form = element.find('#loginForm')

    class Mode
      @switch: ->
        if Mode.current == LoginMode.instance
          Mode.current = RegisterMode.instance
        else
          Mode.current = LoginMode.instance
        Mode.current.enter()
      constructor: (@name) ->
      leave: ->
        scope[@name] = false
      enter: ->
        m.instance.leave() for m in [
          LoginMode
          RegisterMode
          RestoreMode
        ]
        @hideError()
        @hideInfo()
        Mode.current = @
        scope[@name] = true
      showInfo: (htmlText) ->
        @hideError()
        scope.info = htmlText
      hideInfo: ->
        scope.info = null
      showError: (htmlText) ->
        @hideInfo()
        scope.error = htmlText
      hideError: ->
        scope.error = null
      showResultNotification: (r) =>
        if r.success
          @showInfo r.result
        else
          @showError r.result

    class LoginMode extends Mode
      @instance: new LoginMode
      constructor: ->
        super('LoginMode')
      submit: ->
        @hideError()
        scope.sendData(
          email: scope.email
          password: scope.passwd
        , (r) =>
          if !r.success
            @showError(r.result)
        )

    class RegisterMode extends Mode
      @instance: new RegisterMode
      constructor: ->
        super('RegisterMode')
      submit: ->
        scope.sendData(
          email: scope.email
        , @showResultNotification
        )

    class RestoreMode extends Mode
      @instance: new RestoreMode
      constructor: ->
        super('RestoreMode')
      submit: ->
        scope.sendData(
          email: scope.email
          restore: true
        , @showResultNotification
        )

    LoginMode.instance.enter()

    scope.submit = ->
      Mode.current.submit()

    scope.switchMode = ->
      Mode.switch()

    scope.restorePassword = ->
      RestoreMode.instance.enter()

    scope.register = ->
      RegisterMode.instance.enter()

    scope.openForm = ->
      form.fadeIn('fast')

    scope.closeForm = ->
      form.fadeOut 'fast', ->
        if Mode.current != LoginMode.instance
          LoginMode.instance.enter()
        scope.form.$setPristine()
        scope.email = ''
        scope.passwd = ''

]

module.directive 'registerForm', [->
  (scope, element, attrs) ->
    scope.model = {}
    scope.doRegister = ->
      scope.sendData(scope.model, (r) ->
        if r.success
          window.location.href = '/'
          scope.registrationDone = true
      )
]

module.directive 'passwordResetForm', [->
  (scope, element, attrs) ->
    scope.model = {}
    scope.send = ->
      scope.sendData(scope.model, (r) ->
        if r.success
          window.location.href = '/'
      )
]

module.directive 'logoutHandler', ['$rootScope', (rootScope) ->
  (scope, element, attrs) ->
    scope.logout = ->
      scope.sendData {}
]

###
  User menu reloading facilities
###
module.directive 'userMenu', ['$location', '$rootScope', '$http', '$compile', (loc, rootScope, http, compile) ->
  (scope, element, attrs) ->
    rootScope.reloadUserMenu = ->
      http.get('/user_menu.tmpl').success (data) ->
        element.html(compile(data)(scope))
    scope.$watch 'location.path()', (newPath, oldPath) ->
      newPath = loc.canonize(newPath)
      oldPath = loc.canonize(oldPath)
      sameParts = 0
      while sameParts < newPath.length && sameParts < oldPath.length && oldPath[sameParts] == newPath[sameParts]
        sameParts += 1
      if sameParts == 0
        setTimeout scope.reloadUserMenu, 10
]

###
  Create popup material logic
###
module.directive 'popupMaterial', ['$log', '$rootScope', (log, rootScope) ->
  (scope, element, attrs) ->
    element.on 'hidden.bs.modal', ->
      scope.popupMaterial = null

    scope.send = ->
      scope.sendData [scope.popupMaterial], (r) ->
        if (r.success)
          rootScope.$emit 'material.add', r.result
          element.modal 'hide'
]

###
  Directive to add to element to help to update the window title with the partial update of the page
###
module.directive 'dynamicTitle', ['$window', '$log', (window, log) ->
  (scope, element, attrs) ->
    window.document.title = element.text()
]

module.directive 'scrollTop', ['$log', (log) ->
  (scope, element, attrs) ->
    scope.$root.$on '$locationChangeSuccess', (e, newPath) ->
      element.attr 'href', newPath
    scope.scrollTop = (e) ->
      e.preventDefault()
      angular.element('html, body').animate scrollTop: 0, 'fast'
      false
]

module.directive 'tooltipButton', ['$log', (log) ->
  (scope, element, attrs) ->
    element.tooltip()
]

module.directive 'affix', ['$log', (log) ->
  (scope, element, attrs) ->
    element.affix offset: top: attrs.offsetTop
]

module.filter 'linearize', ['$log', (log) ->
  (input) ->
    input.replace /(\n|\s)+/g, " "
]

