module = angular.module 'lifepro'

module.directive 'materialList', ['$log', '$window', '$rootScope', (log, window, rootScope) ->
  (scope, element, attrs) ->
    element.addClass 'material-list'
    listFinished = false

    if attrs.listenAdd != "false"
      rootScope.$on 'material.add', (e, m) ->
        definition = scope.context.shift()
        scope.context.unshift m
        scope.context.unshift definition

    scope.workout = (e) ->
      elem = angular.element e.target

      s = elem.scope()
      while s = s.$$nextSibling
        s.displayWorkoutMenu = false
      s = elem.scope()
      while s = s.$$prevSibling
        s.displayWorkoutMenu = false
      s = elem.scope()

      if !elem.attr 'href'
        s.displayWorkoutMenu = true

    scope.remove = (e) ->
      elem = angular.element e.target
      s = elem.scope()
      s.removing = true
      s.sendData remove: s.material._id, (r) ->
        if (r.success)
          s.removing = false
          s.removed = true
          checkForListUpfetch()

    scope.restore = (e) ->
      elem = angular.element e.target
      s = elem.scope()
      s.sendData restore: s.material._id, (r) ->
        if (r.success)
          s.removed = false

    # loads the list as it is scrolled down
    checkForListUpfetch = () ->
      lastLi = element.find('li').last()
      needFetchNext = window.scrollY + window.innerHeight > lastLi.offset().top
      if needFetchNext && !listFinished && !scope.roundtripInProgress
        scope.sendData {}, (response) ->
          if response.success
            listFinished = response.result.length == 0
            scope.context.push response.result...
    angular.element(window).bind 'scroll', checkForListUpfetch
    element.bind '$destroy', () ->
      angular.element(window).unbind 'scroll', checkForListUpfetch
]