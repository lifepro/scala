module = angular.module 'lifepro'

# Commons

autoSaveText = (scope, varName, data) ->
  timeout = -1
  doSave = ->

  scope.$root.$on '$locationChangeStart', (e) ->
    doSave()

  clear = () ->
    clearTimeout(timeout)
    timeout = -1
    doSave = ->

  saveText = (v) ->
    v = if (v?) then v else ""
    dataToSend = if data?
      data(v)
    else
      v
    scope.sendData dataToSend, (v) ->
      if (v.success)
        scope.flashInfo = v.result
      else
        scope.error = v.result

  scope.$watch varName, (v, oldV) ->
    if (v == oldV || scope.autoSaveDisabled) then return
    clear()
    doSave = ->
      saveText(v)
      clear()
    saveOnScope = -> scope.$apply -> doSave()
    timeout = setTimeout(saveOnScope, 2000)

module.directive 'autoSaveText', ['$log', (log) ->
  (scope, element, attrs) ->
    varName = attrs['autoSaveText']
    autoSaveText scope, varName
]

# Definition (aka MVZ)

module.directive 'definition', ['$log', (log) ->
  (scope, element, attrs) ->
    autoSaveText scope, 'context.text', (v) ->
      definition: v
]

module.controller 'CompleteDefinitionController', ['$scope', '$log', (scope, log) ->
  scope.completeDefinition = () ->
    scope.sendData {}, () ->
      scope.context.completed = true
      delete scope.context.estimation
]

module.controller 'OldTextController', ['$scope', '$log', (scope, log) ->
  resetText = ->
    prevVal = null
    scope.oldText = scope.context.text
    while scope.oldText != prevVal
      prevVal = scope.oldText
      scope.oldText = scope.oldText.replace '\n', '<br/>'
    scope.context.text = ""

  scope.$watch 'context.estimation', (newVal, oldVal) ->
    log.info newVal
    resetText() if newVal? && newVal != 10 && newVal != oldVal

  e = scope.context.estimation
  resetText() if e? && e != 10
]

module.controller 'EstimateController', ['$scope', '$log', (scope, log) ->
  scope.estimate = (ev) ->
    n = parseInt(angular.element(ev.target).text())
    if !isNaN(n)
      scope.sendData n, () ->
        scope.context.estimation = n
        delete scope.context.completed
]

# GSRP

module.controller 'ConfirmBodyController', ['$scope', '$log', (scope, log) ->
  scope.confirmBody = (again) ->
    if again
      scope.sendData {},
      (res) -> if res.success
        scope.context.bodyConfirmed = true
    else
      scope.bodyConfirmed = true
  scope.thinkAgain = () ->
    scope.bodyConfirmed = false
]

module.directive 'gsrpEstimate', ['$log', (log) ->
  (scope, element, attrs) ->
    scope.$watch 'context.gsrp.estimation', (v) ->
      element.find('button').removeClass 'active'
      if v?
        q = element.find('button:contains(' + v + ')').filter () ->
          e = angular.element(@)
          v == parseInt e.text()
        q.addClass('active')
        scope.context.feelingsWishesConfirmed = true
    scope.estimate = (ev) ->
      n = parseInt(angular.element(ev.target).text())
      if !isNaN(n)
        scope.sendData n, () -> scope.context.gsrp.estimation = n
]

module.directive 'confirm', ['$log', (log) ->
  (scope, element, attrs) ->
    varName = attrs['confirm']
    scope.confirm = () ->
      scope.sendData {},
      (res) -> if res.success
        scope.$eval varName + ' = true'
]

module.controller 'CouldChangeController', ['$scope', '$log', (scope, log) ->
  scope.couldChange = (could) ->
    scope.sendData could, () ->
      s = scope.$parent.$parent
      s.bodyConfirmed = false
      s.context = {}
]

module.directive 'estimation', ['$log', (log) ->
  (scope, element, attrs) ->
    varName = attrs['estimation']
    storage = scope[varName]
    scope.$watch ->
      storage[scope.$index]
    , (newVal) ->
      buttons = element.find('button').removeClass('active')
      if newVal
        buttons.filter ->
          angular.element(@).text() == newVal
        .addClass 'active'
    scope.estimate = (e) ->
      storage[scope.$index] = angular.element(e.target).text()
]

module.directive 'autoRows', ['$log', (log) ->
  (scope, element, attrs) ->
    minHeight = 120;
    scope.$watch 'context.text', (v) ->
      textarea = element[0]
      element.height(minHeight)
      element.height(textarea.scrollHeight + 20)
]
