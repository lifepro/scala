package org.lifepro.site
package model

import org.specs2.mutable.Specification
import com.foursquare.rogue.LiftRogue._
import net.liftweb.mongodb.{MongoHost, MongoAddress, DefaultMongoIdentifier, MongoDB}
import com.mongodb.MongoException.DuplicateKey

class UserSpec extends Specification {

  step {
    MongoDB.defineDb(DefaultMongoIdentifier, MongoAddress(MongoHost("127.0.0.1"), "lifepro-test"))
    success
  }

  "User" should {
    "be able to be saved" in {
      User.createRecord.email("alun@katlex.com").firstName("Alexey").lastName("Lunacharsky").save(true)
      success
    }
    "be unable to register single email twice" in {
      User.createRecord.email("alun@katlex.com").firstName("Kate").lastName("Lunacharsky").
        save(true) must throwA[DuplicateKey]
    }
    "be found by email" in {
      User where (_.email eqs "alun@katlex.com") fetch() must have size(1)
    }
    "have correct name" in {
      (User where (_.email eqs "alun@katlex.com") get() : @unchecked) match {
        case Some(u) =>
          u.firstName.is must_== "Alexey"
          u.lastName.is must_== "Lunacharsky"
      }
    }
  }

  step {
    MongoDB.use(DefaultMongoIdentifier)(_.dropDatabase())
    success
  }

}
