import sbt._

import sbt.Keys._
import scala.Some
import spray.revolver.RevolverPlugin._

object BuildSettings {
  val buildOrganization = "org.lifepro"
  val buildScalaVersion = "2.10.2"
  val snapshot = false
  val buildVersion = "1.0.4" + (if (snapshot) "-SNAPSHOT" else "")
  val buildSettings = Defaults.defaultSettings ++ Seq(
    organization := buildOrganization,
    scalaVersion := buildScalaVersion,
    version := buildVersion
  )
}

object BuildSpec extends Build {

  import BuildSettings._

  val liftVersion = "2.5.1"

  lazy val root = Project("lifepro-scala-root", file(".")) aggregate (site)

  lazy val site = Project("lifepro-site", file("site"),
    settings = Revolver.settings ++ buildSettings ++ Seq(
      libraryDependencies ++= Seq(
        "org.specs2" %% "specs2" % "2.2" % "test",
        "org.mortbay.jetty" % "jetty" % "6.1.22" % "compile",
        "ch.qos.logback" % "logback-classic" % "1.0.13" % "compile->default",
        "org.scala-sbt" % "launcher-interface" % "0.12.0" % "provided",
        "net.liftweb" %% "lift-webkit" % liftVersion % "compile->default",
        "net.liftweb" %% "lift-mongodb-record" % liftVersion % "compile->default",
        "com.google.javascript" % "closure-compiler" % "r2180",
        "org.mozilla" % "rhino" % "1.7R3",
        "com.foursquare" %% "rogue-field" % "2.2.0" intransitive(),
        "com.foursquare" %% "rogue-core" % "2.2.0" intransitive(),
        "com.foursquare" %% "rogue-lift" % "2.2.0" intransitive(),
        "com.foursquare" %% "rogue-index" % "2.2.0" intransitive(),
        "me.lessis" %% "lesst" % "0.1.2",
        "eu.henkelmann" % "actuarius_2.10.0" % "0.2.6"
      ) map (_.withSources),
      resolvers += "softprops-maven" at "http://dl.bintray.com/content/softprops/maven",
      Revolver.enableDebugging(port = 4000),
      watchSources <<= (watchSources, unmanagedResources in Compile) map { (ws, rs) =>
        ws.filter(f => !rs.exists(_.getAbsolutePath == f.getAbsolutePath))
      },
      fullClasspath in Runtime <<=
        (fullClasspath in Runtime, unmanagedResourceDirectories in Compile).map { (oldCP, resourceDirs) =>
          resourceDirs.map(x => Attributed.blank(x)) ++ oldCP
        },
      javaOptions in Revolver.reStart += "-Drun.mode=development",
      mainClass in run in Compile := Some("org.lifepro.site.boot.Server"),
      publishTo := Some {
        val target = if (BuildSettings.snapshot) "snapshots" else "releases"
        Resolver.file("katlex-repo", file(sys.props("user.home") + "/katlex.github.com/maven2/" + target))
      }
    )
  )
}
